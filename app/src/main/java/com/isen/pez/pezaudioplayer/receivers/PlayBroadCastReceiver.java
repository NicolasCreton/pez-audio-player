package com.isen.pez.pezaudioplayer.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.app.Notification;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.asyncTask.SeekbarUpdate;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.MusicFragment;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.PlayerFragment;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.QueueFragment;
import com.isen.pez.pezaudioplayer.main_activity.MainActivity;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.music.MusicPlayerState;
import com.isen.pez.pezaudioplayer.music.StateMediaPlayer;
import com.isen.pez.pezaudioplayer.notification.MainNotificationHelper;

import java.io.File;

/**
 * Created by nicolas on 07/11/2015.
 */
public class PlayBroadCastReceiver extends BroadcastReceiver {

    private MusicFragment mMusicFragment;
    private QueueFragment mQueueFragment;
    private PlayerFragment mPlayerFragment;
    private ImageView coverMini;
    private TextView songNameTextView;
    private TextView albumNameTextView;
    private ImageView coverFull;
    private SeekBar seekBar;
    private ImageView playPause;
    private ImageView playButtonFull;

    private Song currentSong;
    private File currentFile;

    private boolean isInitMusicFragment = false;
    private boolean isInitQueueFragment = false;
    private SeekbarUpdate seekbarUpdate;

    public PlayBroadCastReceiver(PlayerFragment playerFragment, MusicFragment musicFragment, QueueFragment queueFragment) {
        this.mPlayerFragment = playerFragment;
        this.mMusicFragment = musicFragment;
        this.mQueueFragment = queueFragment;
        initViews();
    }

    public void initViews() {
        initPlayerFragmentViews();
        initMusicFragmentViews();
        initQueueFragmentViews();
    }

    public void initPlayerFragmentViews() {
        View rootView = mPlayerFragment.getView();
        this.coverMini = (ImageView) rootView.findViewById(R.id.albumSliderImage);
        this.songNameTextView = (TextView) rootView.findViewById(R.id.songNameText);
        this.albumNameTextView = (TextView) rootView.findViewById(R.id.albumNameText);
        this.playPause = (ImageView) rootView.findViewById(R.id.playButton);
    }

    public void initMusicFragmentViews() {
        View rootView = mMusicFragment.getView();
        if (rootView != null) {
            this.coverFull = (ImageView) rootView.findViewById(R.id.albumFullImage);
            this.seekBar = (SeekBar) rootView.findViewById(R.id.seekBarSong);
            this.playButtonFull = (ImageView) rootView.findViewById(R.id.playPauseFullPlayer);
            if (this.coverFull == null || this.seekBar == null || this.playButtonFull == null) {
                isInitMusicFragment = false;
            }
            else {
                isInitMusicFragment = true;
            }
        }
    }

    public void initQueueFragmentViews() {
        isInitQueueFragment = false;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        /*String path = intent.getStringExtra("pathFile");
        if (path != null) {
            onPlay(path);
        }
        else {
            onStop();
        }*/

        MusicFragment musicFragment = mPlayerFragment.getPagerAdapter().getmMusicFragment();
        QueueFragment queueFragment = mPlayerFragment.getPagerAdapter().getmQueueFragment();

        if (musicFragment != mMusicFragment || !isInitMusicFragment) {
            mMusicFragment = musicFragment;
            initMusicFragmentViews();
        }

        if (queueFragment != mQueueFragment) {
            mQueueFragment = queueFragment;
            initQueueFragmentViews();
        }

        onReceive();
    }

    public void onReceive() {
        Song song = MusicPlayer.getQueue().getCurrentSong();
        File file;
        if (song == null) {
            file = MusicPlayer.getQueue().getCurrentFile();
            if (file != null) {
                if (file == currentFile) {
                    onPlay(file.getPath(), true);
                }
                else {
                    currentFile = file;
                    onPlay(file.getPath(), false);
                }

            }
            else {
                onPause();
            }
        } else {
            if (song == currentSong) {
                onPlay(song.getmPath(), true);
            }
            else {
                currentSong = song;
                onPlay(song.getmPath(), false);
            }

        }
        mQueueFragment.notifyDataSetChanged();
    }



    private void setImage(MediaMetadataRetriever mediaMetadataRetriever, String artist, String album, ImageView... views) {
        byte[] artWork;
        artWork = mediaMetadataRetriever.getEmbeddedPicture();
        if (artWork != null) {
            Bitmap cover = BitmapFactory.decodeByteArray(artWork, 0, artWork.length);
            for (ImageView view : views) {
                if (view != null) {
                    view.setImageBitmap(cover);
                }
            }
        } else {
            String url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(artist + album, null);
            if (url == null) {
                for (ImageView view : views) {
                    if (view != null) {
                        PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(view);
                    }
                }
            } else {
                AQuery androidQuery = new AQuery(PezApplication.getContext());
                for (ImageView view : views) {
                    if (view != null) {
                        androidQuery.id(view).image(url, false, true, 200, 0);
                    }
                }
            }
        }
    }

    private String getArtist(MediaMetadataRetriever mediaMetadataRetriever) {
        return mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
    }

    private String getAlbum(MediaMetadataRetriever mediaMetadataRetriever) {
        return mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
    }

    private String getSong(MediaMetadataRetriever mediaMetadataRetriever) {
        return mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
    }

    private void onPause() {
        songNameTextView.setText("");
        albumNameTextView.setText("");
        setPlayImage(R.drawable.ic_play_filled, this.playPause, this.playButtonFull);
        if (coverFull != null) {
            PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(coverFull);
        }

        if (coverMini != null) {
            PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(coverMini);
        }
    }

    public void onPlay(String path, boolean isSameSong) {
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(path);

        String songName = getSong(mediaMetadataRetriever);
        String artist = getArtist(mediaMetadataRetriever);
        String album = getAlbum(mediaMetadataRetriever);

        if (!isSameSong) {
            setImage(mediaMetadataRetriever, artist, album, coverFull, coverMini);
        }


        songNameTextView.setText(songName);
        albumNameTextView.setText(album + " - " + artist);

        if (MusicPlayer.getMediaPlayer().isState_Or(MusicPlayerState.STARTED, MusicPlayerState.PREPARING, MusicPlayerState.PREPARED)) {
            startSeekbar();
            setPlayImage(R.drawable.ic_pause_filled, this.playPause, this.playButtonFull);
        } else {
            setPlayImage(R.drawable.ic_play_filled, this.playPause, playButtonFull);
        }
    }

    public void setPlayImage(int resource, ImageView... views) {
        for (ImageView view : views) {
            if (view != null) {
                view.setImageResource(resource);
            }
        }
    }

    public void startSeekbar() {
        if (this.seekbarUpdate != null) {
            seekbarUpdate.stop();
        }
        this.seekbarUpdate = new SeekbarUpdate(seekBar);
        seekbarUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MusicPlayer.getMediaPlayer());
    }

/*    public void update() {
        String path = null;
        Song song = MusicPlayer.getQueue().getCurrentSong();
        if (song != null) {
            path = song.getmPath();
        }
        else {
            File file = MusicPlayer.getQueue().getCurrentFile();
            if (file != null) {
               path = file.getPath();
            }
        }
        if (path != null) {
            updatePlayer(path);
        }

    }*/

    /*private void updatePlayer(String path) {


        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(path);
        String songName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String albumName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        String artistName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        byte[] artWork;
        artWork = mediaMetadataRetriever.getEmbeddedPicture();
        if (artWork != null){
            Bitmap cover = BitmapFactory.decodeByteArray(artWork, 0, artWork.length);
            coverMini.setImageBitmap(cover);
            coverFull.setImageBitmap(cover);
        } else {
            String url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(artistName + albumName, "NotFound");
            if (url == "NotFound") {
                PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(coverFull);
                PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(coverMini);
            } else {
                AQuery androidQuery = new AQuery(PezApplication.getContext());
                androidQuery.id(coverFull).image(url, false, true, 200, 0);
                androidQuery.id(coverMini).image(url, false, true, 200, 0);
            }
        }
        songNameTextView.setText(songName);
        albumNameTextView.setText(albumName);

        MainNotificationHelper.notify(PezApplication.getContext());
    }

    private void onPlay(String path) {
        ImageView playPause = (ImageView)rootView.findViewById(R.id.playButton);
        ImageView playPauseFull = (ImageView)rootView.findViewById(R.id.playPauseFullPlayer);

        SeekBar seekBar = (SeekBar)rootView.findViewById(R.id.seekBarSong);
        playPause.setImageResource(R.drawable.ic_pause_filled);
        playPauseFull.setImageResource(R.drawable.ic_pause_filled);

        updatePlayer(path);

        //int duration = MusicPlayer.getMediaPlayer().getDuration();
        //seekBar.setMax(duration);
        SeekbarUpdate seekbarUpdate = new SeekbarUpdate(seekBar);
        seekbarUpdate.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MusicPlayer.getMediaPlayer());
    }

    private void onStop() {
        ImageView playPause = (ImageView)rootView.findViewById(R.id.playButton);
        ImageView playPauseFull = (ImageView)rootView.findViewById(R.id.playPauseFullPlayer);
        playPause.setImageResource(R.drawable.ic_play_filled);
        playPauseFull.setImageResource(R.drawable.ic_play_filled);
    }*/
}

/*
MusicPlayerState musicPlayerState =  MusicPlayer.getMediaPlayer().getState();
ImageView playPause = (ImageView)rootView.findViewById(R.id.playButton);
if (musicPlayerState == MusicPlayerState.PAUSED){
        playPause.setImageResource(R.drawable.ic_play_filled);
        }else if (musicPlayerState == MusicPlayerState.STARTED){
        playPause.setImageResource(R.drawable.ic_pause_filled);
        }*/

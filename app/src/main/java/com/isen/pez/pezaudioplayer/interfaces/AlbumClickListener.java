package com.isen.pez.pezaudioplayer.interfaces;

import com.isen.pez.pezaudioplayer.model.Album;

/**
 * Created by franc on 07/11/2015.
 */
public interface AlbumClickListener {
    void onAlbumClick(Album album);
}

package com.isen.pez.pezaudioplayer.fragments.player_fragment.swipe_drag_interfaces;

/**
 * Created by cdupl on 11/10/2015.
 */
public interface ItemTouchHelperAdapter {
    boolean onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}

package com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ITunesSearchResultItem {
    @SerializedName("artistId")
    public int artistId;

    @SerializedName("artistName")
    public String artistName;
}

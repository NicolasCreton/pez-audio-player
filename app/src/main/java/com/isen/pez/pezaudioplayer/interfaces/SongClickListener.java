package com.isen.pez.pezaudioplayer.interfaces;

import android.database.Cursor;

import com.isen.pez.pezaudioplayer.model.Song;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public interface SongClickListener {
    void playSong(Song song, ArrayList<Song> songs);
    void playSongFromQueue(Song song);
}

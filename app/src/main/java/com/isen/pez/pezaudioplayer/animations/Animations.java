package com.isen.pez.pezaudioplayer.animations;

import android.app.Activity;
import android.app.FragmentTransaction;

import com.isen.pez.pezaudioplayer.R;

/**
 * Created by cdupl on 11/11/2015.
 */
public class Animations {
    /**
     * Will be applied on both add and remove
     * @param transaction
     */
    public static void applyFragmentAnimation(FragmentTransaction transaction) {
        transaction.setCustomAnimations(R.animator.custom_fade_in, R.animator.custom_fade_out,
                R.animator.custom_fade_in, R.animator.custom_fade_out);
        /*transaction.setCustomAnimations(R.animator.slide_in_right, R.animator.slide_out_left, R.animator.slide_in_left, R.animator.slide_out_right);*/
    }

    public static void applyBottomAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.custom_slide_in_bottom, R.anim.custom_slide_out_bottom);
    }

    public static void applyUpAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.custom_slide_in_up, R.anim.custom_slide_out_up);
    }

    public static void applyRightAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.custom_slide_in_right, R.anim.custom_slide_out_right);
    }

    public static void applyLeftAnimation(Activity activity) {
        activity.overridePendingTransition(R.anim.custom_slide_in_left, R.anim.custom_slide_out_left);
    }
}

package com.isen.pez.pezaudioplayer.fragments.albums_fragment;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.interfaces.AlbumClickListener;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.utils.CursorRecyclerViewAdapter;

/**
 * Created by franc on 06/11/2015.
 */
public class AlbumsCursorAdapter extends CursorRecyclerViewAdapter<AlbumsRecyclerHolder> {

    private AlbumClickListener mAlbumClickListener;

    public AlbumsCursorAdapter(Context context, Cursor cursor, AlbumClickListener albumClickListener) {
        super(context, cursor);
        mAlbumClickListener = albumClickListener;
    }

    @Override
    public void onBindViewHolder(AlbumsRecyclerHolder viewHolder, Cursor cursor) {
        viewHolder.bindAlbum(Album.albumFromCursor(cursor));
    }

    @Override
    public AlbumsRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_album, parent, false);
        return new AlbumsRecyclerHolder(view, mAlbumClickListener);
    }
}

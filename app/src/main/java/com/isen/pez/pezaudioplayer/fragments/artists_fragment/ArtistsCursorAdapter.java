package com.isen.pez.pezaudioplayer.fragments.artists_fragment;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.interfaces.ArtistClickListener;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.utils.CursorRecyclerViewAdapter;

/**
 * Created by franc on 06/11/2015.
 */
public class ArtistsCursorAdapter extends CursorRecyclerViewAdapter<ArtistsRecyclerHolder> {

    private ArtistClickListener mArtistClickListener;

    public ArtistsCursorAdapter(Context context, Cursor cursor, ArtistClickListener artistClickListener) {
        super(context, cursor);
        mArtistClickListener = artistClickListener;
    }

    @Override
    public void onBindViewHolder(ArtistsRecyclerHolder viewHolder, Cursor cursor) {
        viewHolder.bindArtist(Artist.artistFromCursor(cursor));
    }

    @Override
    public ArtistsRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_artist, parent, false);
        return new ArtistsRecyclerHolder(view, mArtistClickListener);
    }
}

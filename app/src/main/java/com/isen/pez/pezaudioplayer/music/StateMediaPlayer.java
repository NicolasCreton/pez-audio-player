package com.isen.pez.pezaudioplayer.music;

import android.content.Context;
import android.media.MediaDataSource;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;

import com.isen.pez.pezaudioplayer.BroadcastManager;
import com.isen.pez.pezaudioplayer.model.Song;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.Map;

/**
 * Created by cdupl on 11/6/2015.
 */
public class StateMediaPlayer extends MediaPlayer implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    private MusicPlayerState mState = MusicPlayerState.IDLE;
    private String mDataSource;
    private Song currentSong;

    @Override
    public void reset() {
        super.reset();
        mState = MusicPlayerState.IDLE;
    }

    @Override
    public void release() {
        super.release();
    }

    @Override
    public void setDataSource(Context context, Uri uri) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(context, uri);
        mDataSource = uri.getPath();
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void setDataSource(Context context, Uri uri, Map<String, String> headers) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(context, uri, headers);
        mDataSource = uri.getPath();
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void setDataSource(String path) throws IOException, IllegalArgumentException, SecurityException, IllegalStateException {
        super.setDataSource(path);
        mDataSource = path;
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void setDataSource(FileDescriptor fd) throws IOException, IllegalArgumentException, IllegalStateException {
        super.setDataSource(fd);
        mDataSource = null;
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void setDataSource(FileDescriptor fd, long offset, long length) throws IOException, IllegalArgumentException, IllegalStateException {
        super.setDataSource(fd, offset, length);
        mDataSource = null;
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void setDataSource(MediaDataSource dataSource) throws IllegalArgumentException, IllegalStateException {
        super.setDataSource(dataSource);
        mDataSource = null;
        mState = MusicPlayerState.INITIALIZED;
    }

    @Override
    public void prepare() throws IOException, IllegalStateException {
        super.prepare();
        mState = MusicPlayerState.PREPARED;
        broadcastUpdate();

    }

    @Override
    public void prepareAsync() throws IllegalStateException {
        super.prepareAsync();
        mState = MusicPlayerState.PREPARING;
        broadcastUpdate();
    }

    @Override
    public void start() throws IllegalStateException {
        super.start();
        mState = MusicPlayerState.STARTED;
        broadcastUpdate();
    }

    @Override
    public void stop() throws IllegalStateException {
        try {
        super.stop();
        mState = MusicPlayerState.STOPPED;
        }
        catch(IllegalStateException e) {
            e.printStackTrace();
            //Meaning it couldn't be stopped ?
        }
        broadcastUpdate();
    }

    @Override
    public void pause() throws IllegalStateException {
        super.pause();
        mState = MusicPlayerState.PAUSED;
        broadcastUpdate();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mState = MusicPlayerState.PLAYBACK_COMPLETED;
        broadcastUpdate();
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        Log.e("MediaPlayerError", "what : " + what + " ; extra : " + extra);
        if (mState == MusicPlayerState.PREPARING && mDataSource != null) {
            //this.reset();
        }
        else {
            //this.reset();
        }
        return true;
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        mState = MusicPlayerState.PREPARED;
        broadcastUpdate();
    }

    public MusicPlayerState getState() {
        return mState;
    }

    public boolean isState(MusicPlayerState state) {
        return state == mState;
    }

    public boolean isState_Or(MusicPlayerState state1, MusicPlayerState... states) {
        boolean result = mState == state1;
        for (MusicPlayerState state : states) {
            result = result || (state == mState);
        }
        return result;
    }

    @Override
    public int getDuration() {
        try {
        if (isState_Or(MusicPlayerState.STARTED, MusicPlayerState.PAUSED)) {
            return super.getDuration();
        }
        return 0;
        }
        catch(Exception e) {
            return 0;
        }
    }

    @Override
    public int getCurrentPosition() {
        if (!isState(MusicPlayerState.ERROR)) {
            return super.getCurrentPosition();
        }
        return 0;

    }

    public void broadcastUpdate() {
        BroadcastManager.sendPlaySongBroadcast();
    }
}

package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.main_activity.content.MainFragment;
import com.isen.pez.pezaudioplayer.main_activity.music_bar.MusicController;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.music.MusicPlayerState;
import com.isen.pez.pezaudioplayer.music.StateMediaPlayer;
import com.isen.pez.pezaudioplayer.receivers.PlayBroadCastReceiver;
import com.isen.pez.pezaudioplayer.utils.Constants;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

/**
 * Created by nicolas on 06/11/2015.
 */
public class PlayerFragment extends Fragment implements View.OnClickListener {

    private View rootView;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PlayerFragmentPagerAdapter pagerAdapter;

    private ImageView playButton;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private MusicController mMusicController;
    private PlayBroadCastReceiver playBroadCastReceiver;
    private View miniPlayerView;
    private TextView songNameTextView;
    private TextView albumNameTextView;


    public static PlayerFragment newInstance(MusicController musicController) {
        Log.i("MusicController", "" + musicController);
        PlayerFragment fragment = new PlayerFragment();
        fragment.mMusicController = musicController;

        return fragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(playBroadCastReceiver);
    }

    @Override
    public void onResume() {
        super.onResume();
        setReceivers();
        playBroadCastReceiver.onReceive();

        slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {

            private int primaryColor = getResources().getColor(R.color.primary);
            private int whiteColor = getResources().getColor(R.color.white);

            @Override
            public void onPanelSlide(View view, float v) {
                miniPlayerView.setBackgroundColor(primaryColor);
                miniPlayerView.getBackground().setAlpha(Math.round(v * 255));
                if (v < 0.5) {
                    songNameTextView.setTextColor(Color.BLACK);
                    albumNameTextView.setTextColor(Color.BLACK);
                    songNameTextView.setAlpha(1 - v * 2);
                    albumNameTextView.setAlpha(1 - v * 2);
                } else {
                    songNameTextView.setTextColor(Color.WHITE);
                    albumNameTextView.setTextColor(Color.WHITE);
                    songNameTextView.setAlpha((-0.5f + v) * 2);
                    albumNameTextView.setAlpha((-0.5f + v) * 2);
                }
                //playButton.setAlpha(1 - v);

            }

            @Override
            public void onPanelCollapsed(View view) {
                //playButton.setVisibility(View.VISIBLE);
                miniPlayerView.setBackgroundColor(whiteColor);
                songNameTextView.setTextColor(Color.BLACK);
                albumNameTextView.setTextColor(Color.BLACK);

            }

            @Override
            public void onPanelExpanded(View view) {
                //playButton.setVisibility(View.GONE);

                miniPlayerView.setBackgroundColor(primaryColor);

                songNameTextView.setTextColor(Color.WHITE);
                albumNameTextView.setTextColor(Color.WHITE);
                playBroadCastReceiver.onReceive();
            }

            @Override
            public void onPanelAnchored(View view) {

            }

            @Override
            public void onPanelHidden(View view) {

            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.player_fragment, container, false);
        slidingUpPanelLayout = (SlidingUpPanelLayout)getActivity().findViewById(R.id.sliding_layout);
        miniPlayerView = (View)rootView.findViewById(R.id.miniPlayerLayout);
        playButton = (ImageView) rootView.findViewById(R.id.playButton);
        songNameTextView = (TextView)rootView.findViewById(R.id.songNameText);
        albumNameTextView = (TextView)rootView.findViewById(R.id.albumNameText);
        playButton.setOnClickListener(this);

        tabLayout = (TabLayout) rootView.findViewById(R.id.playerFragmentTabLayout);
        viewPager = (ViewPager) rootView.findViewById(R.id.playerFragmentViewPager);
        initViewPagerAndTabs();

        slidingUpPanelLayout.setDragView(miniPlayerView);

        /*FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (musicFragment == null) {
            musicFragment = MusicFragment.getInstance(this, mMusicController);
        }
        transaction.add(R.id.musicFragmentContainer, musicFragment);
        transaction.commit();*/

        return rootView;
    }


    private void initViewPagerAndTabs() {
        tabLayout.addTab(tabLayout.newTab().setText("Player"));
        tabLayout.addTab(tabLayout.newTab().setText("Queue"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        pagerAdapter = new PlayerFragmentPagerAdapter(getChildFragmentManager(), (MusicActivity)getActivity(), this, mMusicController);
        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                //pagerAdapter.update();
                playBroadCastReceiver.onReceive();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == playButton) {
            mMusicController.onPlayPauseClick();
        }
    }

/*    private void changePlayPauseButton(){
        MusicPlayerState musicPlayerState =  MusicPlayer.getMediaPlayer().getState();
        ImageView playPause = (ImageView)rootView.findViewById(R.id.playButton);
        if (musicPlayerState == MusicPlayerState.PAUSED){
            playPause.setImageResource(R.drawable.ic_play_filled);
        }else if (musicPlayerState == MusicPlayerState.STARTED){
            playPause.setImageResource(R.drawable.ic_pause_filled);
        }
    }*/

/*    public void update() {
        changePlayPauseButton();
        updateOthers();
    }*/

    public void setReceivers(){

        playBroadCastReceiver = new PlayBroadCastReceiver(this, pagerAdapter.getmMusicFragment(), pagerAdapter.getmQueueFragment());
        getActivity().registerReceiver(playBroadCastReceiver, new IntentFilter(Constants.BroadcastConstants.PLAY));
    }

    public PlayerFragmentPagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }
}

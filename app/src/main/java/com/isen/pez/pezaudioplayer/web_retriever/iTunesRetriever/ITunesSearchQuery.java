package com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ITunesSearchQuery {
    @SerializedName("results")
    public ArrayList<ITunesSearchResultItem> results;
}

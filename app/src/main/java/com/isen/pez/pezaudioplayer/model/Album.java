package com.isen.pez.pezaudioplayer.model;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

import com.isen.pez.pezaudioplayer.PezApplication;

public class Album {

  public long mAlbumId;
  public String mAlbumName;
  public String mArtistName;
  public int mSongNumber;
  public String mYear;
  public String mAlbumArt;

  public Album(final long albumId, final String albumName, final String artistName,
      final int songNumber, final String albumYear, final String albumArt) {
    super();
    mAlbumId = albumId;
    mAlbumName = albumName;
    mArtistName = artistName;
    mSongNumber = songNumber;
    mYear = albumYear;
    mAlbumArt = albumArt;
  }

  public static Album albumFromCursor(Cursor mCursor){
    final long id = mCursor.getLong(0);
    final String albumName = mCursor.getString(1);
    final String artist = mCursor.getString(2);
    final int songCount = mCursor.getInt(3);
    final String year = mCursor.getString(4);
    String albumArt;
    try {
       albumArt = mCursor.getString(5);
    }
    catch(IllegalStateException e) {
      albumArt = null;
    }

    return new Album(id, albumName, artist, songCount, year, albumArt);
  }

  public Bitmap getImage() {
    return Song.albumImages.get(mAlbumId);
  }

  public void setImage(Bitmap bitmap) {
    Song.albumImages.put(mAlbumId,bitmap);
  }

  public String getImageUrl() {
    String url = Song.albumImageUrls.get(mAlbumId);
    if (url == null) {
      url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(this.mArtistName+this.mAlbumName, null);
    }
    if (url != null) {
      setImageUrl(url);
    }
    return url;
  }

  public void setImageUrl(String url) {
    PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).edit().putString(this.mArtistName + this.mAlbumName, url).commit();
    Song.albumImageUrls.put(mAlbumId,url);
  }
}

package com.isen.pez.pezaudioplayer.asyncTask;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;

import java.util.HashMap;

/**
 * Created by nicolas on 07/11/2015.
 */
public class ConverByteToBitmap extends AsyncTask<Song, Integer, Bitmap> {

    private static HashMap<Long, String> mUrlHashmap = new HashMap<>();

    private static MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
    private static AQuery aQuery = new AQuery(PezApplication.getContext());

    private ImageView imageView;
    private Song song;
    private String url;

    public ConverByteToBitmap(ImageView imageView) {
        this.imageView = imageView;
        imageView.setImageResource(R.drawable.ic_empty_album);
    }

    @Override
    protected Bitmap doInBackground(Song... params) {
        this.song = params[0];
        Bitmap cover = song.getImage();
        if (cover != null) {
            return cover;
        }

        Song currentSong = MusicPlayer.getQueue().getCurrentSong();
        if (currentSong == null || !currentSong.equals(song)) {
            mediaMetadataRetriever.setDataSource(song.getmPath());
            byte[] artWork;
            artWork = mediaMetadataRetriever.getEmbeddedPicture();
            if (artWork != null) {
                cover = BitmapFactory.decodeByteArray(artWork, 0, artWork.length);
                song.setImage(cover);
                return cover;
            }
        }


        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (imageView != null) {
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
            } else {
                String url = song.getImageUrl();
                if (url != null) {
                    song.setImageUrl(url);
                    aQuery.id(imageView).image(url, false, true, 200, 0);
                }
                else {
                    PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(imageView);
                }


                /*else {

                    url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(song.mArtistName + song.mAlbumName, "NotFound");
                    if (url == "NotFound") {
                        PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(imageView);
                    } else {
                        song.setImageUrl(url);
                        aQuery.id(imageView).image(url, false, true, 200, 0);
                    }
                }*/


            }
        }
    }
}

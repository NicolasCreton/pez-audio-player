package com.isen.pez.pezaudioplayer.fragments.file_explorer;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.GridLayoutManager.DefaultSpanSizeLookup;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.interfaces.DirectoryClickListener;
import com.isen.pez.pezaudioplayer.music.HistoryManager;
import com.isen.pez.pezaudioplayer.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by cdupl on 11/4/2015.
 */
public class FileExplorerFragment extends Fragment {

    private HistoryManager historyManager;
    private DirectoryClickListener directoryClickListener;
    private GridLayoutManager manager;

    private RecyclerView mRecyclerView;
    private FileExplorerRecyclerAdapter mAdapter;



    public static FileExplorerFragment newInstance(DirectoryClickListener directoryClickListener) {
        return newInstance(directoryClickListener, HistoryManager.rootDirectory);
    }

    public static FileExplorerFragment newInstance(DirectoryClickListener directoryClickListener, File directory) {
        FileExplorerFragment fragment = new FileExplorerFragment();
        fragment.historyManager = new HistoryManager(directory);
        fragment.setDirectoryClickListener(directoryClickListener);
        return fragment;
    }

    private void setDirectoryClickListener(DirectoryClickListener directoryClickListener) {
        this.directoryClickListener = directoryClickListener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.file_explorer_fragment, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.fileExplorerRecylerView);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        setupRecyclerView(mRecyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        manager = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {

            @Override
            public int getSpanSize(int position) {
                if (position < mAdapter.nbFolders)  {
                    return 1;
                }
                return 2;
            }
        });
        recyclerView.setLayoutManager(manager);
        //recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setDirectory(historyManager.getCurrentDirectory());
    }

    /*private List<String> createItemList() {
        List<String> itemList = new ArrayList<>();
        Bundle bundle = getArguments();
        if(bundle!=null) {
            int itemsCount = bundle.getInt(ITEMS_COUNT_KEY);
            for (int i = 0; i < itemsCount; i++) {
                itemList.add("Item " + i);
            }
        }
        return itemList;
    }*/

    public void setDirectory(String directoryPath) {
        this.setDirectory(new File(directoryPath));
    }

    public void setDirectory(File directoryPath) {
        try {
            setAdapter(Arrays.asList(historyManager.next(directoryPath).listFiles()));
        }
        catch(NullPointerException ex) {
            setAdapter(new ArrayList<File>());
        }

    }

    private void setAdapter(List<File> files) {
       if (mAdapter == null) {
            mAdapter = new FileExplorerRecyclerAdapter(files, directoryClickListener);
            mRecyclerView.setAdapter(mAdapter);
        } else {
            mAdapter.changeDirectory(files);
            mRecyclerView.getLayoutManager().scrollToPosition(0);
        }


    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter = null;
    }

    public boolean previous() {
        if (historyManager.hasPrevious()) {
            setAdapter(Arrays.asList(historyManager.previous().listFiles()));
            return true;
        }
        return false;
    }
}

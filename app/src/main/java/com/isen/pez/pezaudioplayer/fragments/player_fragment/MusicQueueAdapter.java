package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.swipe_drag_interfaces.ItemTouchHelperAdapter;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsRecyclerHolder;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.music.MusicQueue;

/**
 * Created by cdupl on 11/10/2015.
 */
public class MusicQueueAdapter extends RecyclerView.Adapter<MusicQueueRecyclerHolder> implements ItemTouchHelperAdapter{


    private final SongClickListener mSongClickListener;
    private MusicQueue mMusicQueue;

    public MusicQueueAdapter(SongClickListener mSongClickListener) {
        mMusicQueue = MusicPlayer.getQueue();
        this.mSongClickListener = mSongClickListener;
    }

    @Override
    public MusicQueueRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_song, parent, false);
        return new MusicQueueRecyclerHolder(view, mSongClickListener);
    }

    @Override
    public void onBindViewHolder(MusicQueueRecyclerHolder holder, int position) {
        holder.bindSong(mMusicQueue.get(position));
    }

    @Override
    public int getItemCount() {
        return mMusicQueue.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                mMusicQueue.swap(i,i+1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                mMusicQueue.swap(i, i-1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        mMusicQueue.remove(position);
        notifyItemRemoved(position);
    }
}

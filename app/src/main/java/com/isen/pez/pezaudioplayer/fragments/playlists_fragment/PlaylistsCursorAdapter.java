package com.isen.pez.pezaudioplayer.fragments.playlists_fragment;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.interfaces.PlaylistClickListener;
import com.isen.pez.pezaudioplayer.model.Playlist;
import com.isen.pez.pezaudioplayer.utils.CursorRecyclerViewAdapter;

/**
 * Created by ronan on 11/11/15.
 */
public class PlaylistsCursorAdapter extends CursorRecyclerViewAdapter<PlaylistsRecycleHolder> {

    private PlaylistClickListener mPlaylistClickListener;

    public PlaylistsCursorAdapter(Context context, Cursor cursor, PlaylistClickListener playlistClickListener) {
        super(context, cursor);
        mPlaylistClickListener = playlistClickListener;
    }

    @Override
    public void onBindViewHolder(PlaylistsRecycleHolder viewHolder, Cursor cursor) {
        viewHolder.bindPlaylist(Playlist.playlistFromCursor(cursor));
    }

    @Override
    public PlaylistsRecycleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_song, parent, false);//tmp
        return new PlaylistsRecycleHolder(view, mPlaylistClickListener);
    }
}

package com.isen.pez.pezaudioplayer.main_activity;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.animations.Animations;
import com.isen.pez.pezaudioplayer.fragments.albums_fragment.AlbumsFragment;
import com.isen.pez.pezaudioplayer.fragments.artists_fragment.ArtistsFragment;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.PlayerFragment;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsFragment;
import com.isen.pez.pezaudioplayer.main_activity.content.MainFragment;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.model.Song;

import java.io.File;

public class MainActivity extends MusicActivity {

    private MainFragment mMainFragment;
    private PlayerFragment playerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        askPermission();
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        if (mMainFragment == null) {
            mMainFragment = MainFragment.newInstance(this);
        }
        transaction.add(R.id.tabs_fragment_container, mMainFragment);
        playerFragment = PlayerFragment.newInstance(getMusicBarController());
        transaction.add(R.id.playerContainer, playerFragment);

        transaction.commit();
    }

    private void askPermission(){
        if(ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        42);
            }
            finishAffinity();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_about_button) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onDirectoryClick(String dirPath) {
        onDirectoryClick(new File(dirPath));
        //fileExplorerFragment.setDirectory(dirPath);
    }

    @Override
    public void onDirectoryClick(File folder) {
        mMainFragment.setNewDirectory(folder);
    }

    @Override
    public void onBackPressed() {
        if (mMainFragment.getTabLayout().getSelectedTabPosition() == 0 && mMainFragment.getPagerAdapter().getFileExplorerFragment().previous()) {
            //Previous already performed in the condition
        }
        else if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else if (isTaskRoot() && getFragmentManager().getBackStackEntryCount() == 0) {
            askLeave();
        } else {
            super.onBackPressed();
            Animations.applyLeftAnimation(this);
        }
    }
}

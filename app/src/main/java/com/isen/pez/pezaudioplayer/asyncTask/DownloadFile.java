package com.isen.pez.pezaudioplayer.asyncTask;

import android.os.AsyncTask;
import android.os.Environment;

import com.isen.pez.pezaudioplayer.model.Artist;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by franc on 08/11/2015.
 */
public class DownloadFile extends AsyncTask<Artist,Integer,String> {
    private Artist mArtist;
    private String PATH;
    public DownloadFile (Artist artist){
        mArtist = artist;
    }
    @Override
    protected String doInBackground(Artist... artist) {
        int count;
        try {
            URL url = new URL((String) mArtist.getImagePath());
            URLConnection connection = url.openConnection();
            connection.connect();
            String targetFileName=mArtist.getImagePath()+".pez";//Change name and subname
            PATH = Environment.getExternalStorageDirectory()+ "/"+"PezDownloads"+"/";
            File folder = new File(PATH);
            if(!folder.exists()){
                folder.mkdir();//If there is no folder it will be created.
            }
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(PATH+targetFileName);
            byte data[] = new byte[1024];
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
            output.flush();
            output.close();
            input.close();
            return PATH;
        } catch (Exception e) {}
        return null;
    }

    protected void onPostExecute(String path) {
        mArtist.setImagePath(path);
    }
}

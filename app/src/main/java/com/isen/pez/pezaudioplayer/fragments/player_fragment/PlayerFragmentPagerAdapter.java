package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.main_activity.music_bar.MusicController;

import java.util.Queue;

/**
 * Created by cdupl on 11/10/2015.
 */
public class PlayerFragmentPagerAdapter extends FragmentPagerAdapter {

    public static final int NB_TABS = 2;

    private final PlayerFragment mPlayerFragment;
    private final MusicActivity mMusicActivity;
    private MusicFragment mMusicFragment;
    private MusicController mMusicController;
    private QueueFragment mQueueFragment;

    public PlayerFragmentPagerAdapter(FragmentManager manager, MusicActivity mMusicActivity, PlayerFragment mPlayerFragment, MusicController mMusicController) {
        super(manager);
        this.mPlayerFragment = mPlayerFragment;
        this.mMusicActivity = mMusicActivity;
        this.mMusicController = mMusicController;

        mMusicFragment = MusicFragment.newInstance(mPlayerFragment, mMusicController);
        mQueueFragment = QueueFragment.newInstance(mMusicActivity, mPlayerFragment);
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            if (mMusicFragment == null) {
                mMusicFragment = MusicFragment.newInstance(mPlayerFragment, mMusicController);
            }

            return mMusicFragment;
        }
        else {
            if (mQueueFragment == null) {
                mQueueFragment = QueueFragment.newInstance(mMusicActivity, mPlayerFragment);
            }

            return mQueueFragment;
        }
    }

    @Override
    public int getCount() {
        return NB_TABS;
    }

/*    public void update() {
        Log.i("PlayerFragmentAdapter","update");
        if (mMusicFragment != null) {
            //mMusicFragment.update();
        }
        if (mQueueFragment != null) {
            //mQueueFragment.update();
            Log.i("QueueFragment", "update");
        }

    }*/

    public MusicFragment getmMusicFragment() {
        return mMusicFragment;
    }

    public QueueFragment getmQueueFragment() {
        return mQueueFragment;
    }
}

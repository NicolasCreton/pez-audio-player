package com.isen.pez.pezaudioplayer.fragments.file_explorer;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.interfaces.DirectoryClickListener;
import com.isen.pez.pezaudioplayer.R;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cdupl on 11/6/2015.
 */
public class FileExplorerRecyclerAdapter extends RecyclerView.Adapter<FileExplorerRecyclerHolder> {
    private List<File> files;
    public int nbFolders = 0;
    private final DirectoryClickListener directoryClickListener;

    public FileExplorerRecyclerAdapter(List<File> files, DirectoryClickListener directoryClickListener) {
        this.files = sortFiles(files);
        this.directoryClickListener = directoryClickListener;
    }

    @Override
    public FileExplorerRecyclerHolder onCreateViewHolder(ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_explorer_item, parent, false);
        return new FileExplorerRecyclerHolder(view, directoryClickListener);
    }

    @Override
    public void onBindViewHolder(FileExplorerRecyclerHolder fileExplorerRecyclerViewHolder, int i) {
        fileExplorerRecyclerViewHolder.bindFile(files.get(i));
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public void changeDirectory(List<File> files) {
        this.files = sortFiles(files);
        notifyDataSetChanged();
    }

    public List<File> sortFiles(List<File> items) {
        List<File> folders = new LinkedList<>();
        List<File> files = new LinkedList<>();

        for (File item : items) {
            if (item.isDirectory()) {
                folders.add(item);
            } else {
                files.add(item);
            }
        }

        Collections.sort(folders);
        this.nbFolders = folders.size();
        Collections.sort(files);
        folders.addAll(files);
        return folders;
    }
}

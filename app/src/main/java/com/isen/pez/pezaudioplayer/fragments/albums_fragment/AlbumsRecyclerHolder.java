package com.isen.pez.pezaudioplayer.fragments.albums_fragment;

import android.graphics.Bitmap;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.asyncTask.AlbumArt;
import com.isen.pez.pezaudioplayer.asyncTask.ArtistArt;
import com.isen.pez.pezaudioplayer.interfaces.AlbumClickListener;
import com.isen.pez.pezaudioplayer.model.Album;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by franc on 06/11/2015.
 */
public class AlbumsRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final TextView name;
    private ImageView albumArt;
    private Album album;
    private AlbumClickListener mAlbumClickListener;

    public AlbumsRecyclerHolder(View itemView, AlbumClickListener albumClickListener) {
        super(itemView);
        itemView.setOnClickListener(this);
        name = (TextView) itemView.findViewById(R.id.album_name);
        albumArt = (ImageView) itemView.findViewById(R.id.album_art);
        mAlbumClickListener = albumClickListener;
        //R.id
    }

    public void bindAlbum(Album album) {
        this.album = album;
        name.setText(album.mAlbumName);

        Bitmap bitmap = album.getImage();

        if (bitmap != null){
            //PicassoHelper.with(PezApplication.getContext()).load(new File(album.mAlbumArt)).into(albumArt);
            albumArt.setImageBitmap(bitmap);
        } else {
            String url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(album.mArtistName+album.mAlbumName, "NotFound");
            if(url=="NotFound"){
                AlbumArt albumArtAsync = new AlbumArt(album,albumArt);
                albumArtAsync.execute();
            } else {
                AQuery androidQuery = new AQuery(PezApplication.getContext());
                androidQuery.id(albumArt).image(url, false, true,200,0);
            }
        }



    }

    @Override
    public void onClick(View v) {
        mAlbumClickListener.onAlbumClick(album);
    }
}

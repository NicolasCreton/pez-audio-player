package com.isen.pez.pezaudioplayer.music;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

import com.isen.pez.pezaudioplayer.PezApplication;

import java.io.IOException;

/**
 * Created by cdupl on 11/6/2015.
 */
public class MusicPlayerFocusChangeListener implements AudioManager.OnAudioFocusChangeListener {

    private final MediaPlayer mMediaPlayer;

    public MusicPlayerFocusChangeListener(MediaPlayer mMediaPlayer) {
        this.mMediaPlayer = mMediaPlayer;
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        AudioManager am = (AudioManager) PezApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        switch (focusChange) {

            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK):
                // Lower the volume while ducking.
                mMediaPlayer.setVolume(0.2f, 0.2f);
                break;
            case (AudioManager.AUDIOFOCUS_LOSS_TRANSIENT):
                MusicPlayer.pause();
                break;

            case (AudioManager.AUDIOFOCUS_LOSS):
                MusicPlayer.pause();
                //ComponentName component = new ComponentName(AudioPlayerActivity.this, MediaControlReceiver.class);
                //am.unregisterMediaButtonEventReceiver(component);
                break;

            case (AudioManager.AUDIOFOCUS_GAIN):
                // Return the volume to normal and resume if paused.
                mMediaPlayer.setVolume(1f, 1f);
                try {
                    MusicPlayer.play();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}

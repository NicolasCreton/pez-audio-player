package com.isen.pez.pezaudioplayer.model;

import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

/**
 * Created by ronan on 11/11/15.
 */
public class Playlist {
    public long mPlaylistId;
    public String mPlaylistName;
    public String mPlaylistModified;
    private static final String TAG = "Playlist";

    public Playlist(final long playlistId, final String playlistName, String playlistModified) {
        super();
        mPlaylistId = playlistId;
        mPlaylistName = playlistName;
        mPlaylistModified = playlistModified;
    }

    public static Playlist playlistFromCursor(Cursor mCursor){
        final long id = mCursor.getLong(0);
        final String playlistName = mCursor.getString(1);
        String playlistModified = mCursor.getString(2);


        return new Playlist(id, playlistName, playlistModified);
    }

   /* public void addPlaylist(String pname) {

        Uri playlists = MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI;
        Cursor c = resolver.query(playlists, new String[] { "*" }, null, null,
                null);
        long playlistId = 0;
        c.moveToFirst();
        do {
            String plname = c.getString(c
                    .getColumnIndex(MediaStore.Audio.Playlists.NAME));
            if (plname.equalsIgnoreCase(pname)) {
                playlistId = c.getLong(c
                        .getColumnIndex(MediaStore.Audio.Playlists._ID));
                break;
            }
        } while (c.moveToNext());
        c.close();

        if (playlistId != 0) {
            Uri deleteUri = ContentUris.withAppendedId(playlists, playlistId);
            Log.d(TAG, "REMOVING Existing Playlist: " + playlistId);

            // delete the playlist
            resolver.delete(deleteUri, null, null);
        }

        Log.d(TAG,"CREATING PLAYLIST: " + pname);
        ContentValues v1 = new ContentValues();
        v1.put(MediaStore.Audio.Playlists.NAME, pname);
        v1.put(MediaStore.Audio.Playlists.DATE_MODIFIED,
                System.currentTimeMillis());
        Uri newpl = resolver.insert(playlists, v1);
        Log.d(TAG, "Added PlayLIst: " + newpl);
    }*/
}

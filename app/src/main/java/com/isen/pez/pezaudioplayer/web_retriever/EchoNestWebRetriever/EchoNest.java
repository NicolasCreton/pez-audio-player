package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistBiography.Biography;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistBiography.EchoNestArtistBiographyQuery;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistImage.EchoNestImage;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistNews.ArtistNews;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistNews.EchoNestArtistNewsQuery;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch.WebArtistResponse;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistImage.EchoNestArtistImage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNest {
    public static final String apiKey = "MKYNSAF7PYGWTKMZZ";
    public static final String consumerKey = "414a6e112f44137d728ab2969e9b410a";
    public static final String sharedSecret = "4OQ6ht0RTQmVAyyKP1dlCA";

    public static final String apiKeyAttr = "api_key=" + apiKey;

    public static final String EchoNestAPIUrl = "http://developer.echonest.com/api/v4/";
    public static final String EchoNestArtistAPIUrl = EchoNestAPIUrl + "artist/";
    public static final String EchoNestArtistSearchAPIUrl = EchoNestArtistAPIUrl + "search?" + apiKeyAttr + "&name="; ////http://developer.echonest.com/api/v4/artist/search?api_key=FILDTEOIK2HBORODV&name=radiohead

    //http://developer.echonest.com/api/v4/artist/images?api_key=MKYNSAF7PYGWTKMZZ&id=ARH6W4X1187B99274F&format=json&results=1&start=0
    public static final String EchoNestArtistImageAPIUrl = EchoNestArtistAPIUrl + "images?" + apiKeyAttr + "&format=json&start=0&id=";

    //http://developer.echonest.com/api/v4/artist/biographies?api_key=MKYNSAF7PYGWTKMZZ&id=ARH6W4X1187B99274F&format=json
    public static final String EchoNestArtistBiographyAPIUrl = EchoNestArtistAPIUrl + "biographies?" + apiKeyAttr + "&format=json&id=";

    //http://developer.echonest.com/api/v4/artist/news?api_key=MKYNSAF7PYGWTKMZZ&id=ARH6W4X1187B99274F&format=json&results=5
    public static final int NB_ARTICLES = 5;
    public static final String EchoNestArtistNewsAPIUrl = EchoNestArtistAPIUrl + "news?" + apiKeyAttr + "&format=json&results=" + NB_ARTICLES + "&id=";

    public static String getArtistId(String artist) {
        artist = artist.replaceAll(" ","+");
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(EchoNestArtistSearchAPIUrl + artist);
            //If success
            if (connection != null) {
                //Build our artists list
                WebArtistResponse response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), WebArtistResponse.class);
                if (response.response.artists != null && response.response.artists.size() > 0) {
                    return response.response.artists.get(0).id;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static LinkedList<String> getArtistImagePath(String artistId) {
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(EchoNestArtistImageAPIUrl + artistId);
            //If success
            if (connection != null) {
                //Build our artists list
                EchoNestArtistImage response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), EchoNestArtistImage.class);
                if (response.response.images != null && response.response.images.size() > 0) {
                    LinkedList<String> urls = new LinkedList<>();
                    for (EchoNestImage image : response.response.images)
                    {
                        if (image != null && image.url != null) {
                            urls.add(image.url);
                        }
                    }
                    return urls;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Biography getArtistBiography(String artistId) {
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(EchoNestArtistBiographyAPIUrl + artistId);
            //If success
            if (connection != null) {
                //Build our artists list
                EchoNestArtistBiographyQuery response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), EchoNestArtistBiographyQuery.class);
                if (response.response.biographies != null && response.response.biographies.size() > 0) {
                    ArrayList<Biography> biographies = response.response.biographies;
                    for (Biography bio : biographies) {
                        if (bio.site.toLowerCase().equals("wikipedia")) {
                            return bio;
                        }

                    }
                    return biographies.get(0);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<ArtistNews> getArtistNews(String artistId) {
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(EchoNestArtistNewsAPIUrl + artistId);
            //If success
            if (connection != null) {
                //Build our artists list
                EchoNestArtistNewsQuery response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), EchoNestArtistNewsQuery.class);
                if (response.response.news != null && response.response.news.size() > 0) {
                    return response.response.news;
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();

    }
        return null;
}

    private static HttpURLConnection getHTTPUrlConnection(String url) throws Exception {
        Log.i("Echo Nest", url);
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");

        final int responseCode = connection.getResponseCode();
        if (responseCode == 200) {
            Log.i("Echo Nest", "Success");
            return connection;
        }
        return null;
    }

    /**
     * Debug function to print the JSON in adb.
     *
     * @param connection
     */
    private static String printJSON(final HttpURLConnection connection) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            Log.i("printJSON", sb.toString());
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}

package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.media.MediaMetadataRetriever;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.music_bar.MusicController;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.music.MusicPlayerState;
import com.isen.pez.pezaudioplayer.music.StateMediaPlayer;
import com.isen.pez.pezaudioplayer.receivers.PlayBroadCastReceiver;
import com.isen.pez.pezaudioplayer.utils.Constants;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.List;

/**
 * Created by cdupl on 11/10/2015.
 */
public class MusicFragment extends Fragment implements View.OnClickListener {

    private View rootView;

    private PlayerFragment playerFragment;

    private ImageView playButtonFull;
    private ImageView forwardButton, rewindButton, shareButton;
    private SurfaceView surfaceView;
    private MusicController mMusicController;

    private SeekBar seekBar;


    public static MusicFragment newInstance(PlayerFragment playerFragment, MusicController musicController) {
        MusicFragment fragment = new MusicFragment();
        fragment.playerFragment = playerFragment;
        Log.i("MusicFragment", "" + musicController);
        fragment.mMusicController = musicController;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.player_music_fragment, container, false);
        seekBar = (SeekBar)rootView.findViewById(R.id.seekBarSong);

        playButtonFull = (ImageView) rootView.findViewById(R.id.playPauseFullPlayer);
        forwardButton = (ImageView) rootView.findViewById(R.id.forward);
        rewindButton = (ImageView) rootView.findViewById(R.id.rewind);
        shareButton = (ImageView) rootView.findViewById(R.id.shareButton);
        surfaceView = (SurfaceView) rootView.findViewById(R.id.surfaceView);

        MusicPlayer.setSurfaceView(surfaceView);
        playButtonFull.setOnClickListener(this);
        forwardButton.setOnClickListener(this);
        rewindButton.setOnClickListener(this);
        shareButton.setOnClickListener(this);
        //changePlayPauseButton();
        return rootView;
    }


    @Override
    public void onClick(View v) {
        if (v == playButtonFull) {
            Log.i("MusicFragment",""+mMusicController);
            mMusicController.onPlayPauseClick();
            //changePlayPauseButton();
        }
        if(v==forwardButton){
            mMusicController.onNextClick();
        }
        if(v==rewindButton){
            mMusicController.onPreviousClick();
        }
        if(v==shareButton){
            shareMusic();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            private int mProgress;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    //MusicPlayer.getMediaPlayer().seekTo(progress);
                    mProgress = progress;
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MusicPlayer.getMediaPlayer().seekTo(mProgress);

            }
        });
        //update();
    }

/*    public void changePlayPauseButton(){
        if (rootView != null) {
            MusicPlayerState musicPlayerState = MusicPlayer.getMediaPlayer().getState();
            ImageView playPauseFull = (ImageView)rootView.findViewById(R.id.playPauseFullPlayer);
            if (musicPlayerState == MusicPlayerState.PAUSED){
                playPauseFull.setImageResource(R.drawable.ic_play_filled);
            }else if (musicPlayerState == MusicPlayerState.STARTED){
                playPauseFull.setImageResource(R.drawable.ic_pause_filled);
            }
        }
    }*/


    public void shareMusic(){
        String text = "";
        if(MusicPlayer.getQueue().getCurrentSong() !=null){
            text = "I like this music : "+MusicPlayer.getQueue().getCurrentSong().mArtistName+", "+MusicPlayer.getQueue().getCurrentSong().mSongName;
        }
        else if(MusicPlayer.getQueue().getCurrentFile() !=null ){
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(MusicPlayer.getQueue().getCurrentFile().getPath());
            String songName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            String artistName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            text = "I like this music : "+artistName+", "+songName;
        }

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);//android.content.Intent.ACTION_SEND
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "I want to share a music with you");
        String shareText = text;
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText);


        //display apps that support the intent
        List<ResolveInfo> respondingApps = this.getActivity().getPackageManager().queryIntentActivities(sharingIntent, 0);
        for(ResolveInfo ri : respondingApps){
            ComponentName name = new ComponentName(ri.activityInfo.packageName,ri.activityInfo.name);
        }

        startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
    public SurfaceView getSurfaceView() {
        return surfaceView;
    }




/*    public void update() {
        changePlayPauseButton();
    }*/
}
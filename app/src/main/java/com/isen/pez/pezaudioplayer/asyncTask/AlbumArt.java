package com.isen.pez.pezaudioplayer.asyncTask;

import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.widget.ImageView;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.model.Album;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by franc on 10/11/2015.
 */
public class AlbumArt extends AsyncTask<Void, Void, String> {
    private final ImageView mImageView;
    private Album mAlbum;
    private boolean wasLoaded = false;

    public AlbumArt(Album album, ImageView imageView) {
        mAlbum = album;
        mImageView = imageView;
        mImageView.setImageResource(R.drawable.ic_empty_album);
    }

    @Override
    protected String doInBackground(Void... params) {


        String imageUrl = mAlbum.getImageUrl();

        if (imageUrl == null) {
            try {
                URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + mAlbum.mArtistName.replace(" ", "+") + "+" + mAlbum.mAlbumName.replace(" ", "+") + "+cover&rsz=1&tbs=isz:m&tbm=isch");
                URLConnection connection = url.openConnection();
                String line;
                StringBuilder builder = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONObject json = new JSONObject(builder.toString());
                JSONObject json2 = json.getJSONObject("responseData");
                JSONArray json3 = json2.getJSONArray("results");
                JSONObject json4 = json3.getJSONObject(0);
                imageUrl = json4.getString("unescapedUrl");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else {
            wasLoaded = true;
        }
        return imageUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        if (s != null) {
            if (!wasLoaded) {
                mAlbum.setImageUrl(s);
            }
            PicassoHelper.with(PezApplication.getContext()).load(s).placeholder(R.drawable.ic_empty_album).into(mImageView);
        }
    }
}

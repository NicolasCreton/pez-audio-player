package com.isen.pez.pezaudioplayer.music;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MainActivity;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.notification.MainNotificationHelper;
import com.isen.pez.pezaudioplayer.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cdupl on 11/6/2015.
 */
public class MusicPlayerService extends Service {

    private static final String LOG_TAG = "ForegroundService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG_TAG, "onStartCommand");

        if(intent == null || intent.getAction() == null) {
            stopForeground(true);
            stopSelf();
            return START_STICKY;
        }

        if (intent.getAction().equals(Constants.MusicPlayer.PLAY_FILE_PATH_ACTION)) {
            Log.i(LOG_TAG, "Received Start Foreground Intent");
            try {
                String path = intent.getExtras().getString("path");
                if (path != null) {
                    MusicPlayer.play(new File(path));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            //makeNotification();
        } else if (intent.getAction().equals("Previous")) {
            Log.i(LOG_TAG, "Clicked Previous");
            try {
                MusicPlayer.previous();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals(Constants.MusicPlayer.TOGGLE_PLAY_PAUSE_ACTION)) {
            MusicPlayer.togglePlayPause();
        } else if (intent.getAction().equals("Next")) {
            Log.i(LOG_TAG, "Clicked Next");
            try {
                MusicPlayer.next();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals("Notification")) {
            Log.i(LOG_TAG, "Received Stop Foreground Intent");
            stopForeground(true);
            stopSelf();
            return START_STICKY;
        } else if (intent.getAction().equals(Constants.MusicPlayer.PLAY_LIST_SONGS_ACTION)) {
            ArrayList<Song> songs = intent.getParcelableArrayListExtra("other_songs_path");
            try {
                MusicPlayer.play((Song)intent.getParcelableExtra("songPath"), songs);
                //makeNotification();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals(Constants.MusicPlayer.PLAY_SONG_IN_QUEUE)) {
            try {
                MusicPlayer.playSongInQueue((Song)intent.getParcelableExtra("songPath"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        makeNotification();
        return START_STICKY;
    }

    private void makeNotification() {
        MainNotificationHelper.notify(this);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(LOG_TAG, "In onDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        //Used only in case of bound services
        return null;
    }


}

package com.isen.pez.pezaudioplayer.interfaces;

import com.isen.pez.pezaudioplayer.model.Artist;

/**
 * Created by franc on 07/11/2015.
 */
public interface ArtistClickListener {
    void onArtistClick(Artist artist);
}

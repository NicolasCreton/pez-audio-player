package com.isen.pez.pezaudioplayer;

import android.app.Application;
import android.content.Context;

/**
 * Created by franc on 06/11/2015.
 */
public class PezApplication extends Application {
    private static Context sContext;

    public void onCreate(){
        super.onCreate();

        // Keep a reference to the application context
        sContext = getApplicationContext();
    }

    // Used to access Context anywhere within the app
    public static Context getContext() {
        return sContext;
    }
}

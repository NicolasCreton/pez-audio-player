package com.isen.pez.pezaudioplayer.music;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cdupl on 11/4/2015.
 */
public class HistoryManager {

    private List<File> directories = new ArrayList<>();
    private int index = -1;

    private static final String ROOT_DIRECTORY = "/";
    public static final File rootDirectory = new File(ROOT_DIRECTORY);

    public HistoryManager() {
        this(rootDirectory);
    }

    public HistoryManager(File folder) {
        if (folder == null) {
            folder = rootDirectory;
        }

        initialize();
        if (folder.isDirectory()) {
            next(folder);
        }
    }

    public void initialize() {
        directories.clear();
        index = -1;
    }

    public File getCurrentDirectory() {
        return directories.get(index);
    }

    public File next(File directoryPath) {
        if (!directoryPath.isDirectory()) {
            directoryPath = rootDirectory;
        }
        directories.add(directoryPath);
        index++;
        return getCurrentDirectory();
    }

    public File previous() {
        if (index > 0) {
            directories.remove(index);
            index--;
        }
        return getCurrentDirectory();
    }

    public File getPrevious() {
        if (hasPrevious()) {
            return directories.get(index - 1);
        }
        return null;
    }

    public boolean hasPrevious() {
        return index > 0;
    }
}

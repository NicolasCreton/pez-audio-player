package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class WebResponse {
    @SerializedName("status")
    public WebStatus status;

    @SerializedName("artists")
    public ArrayList<WebArtist> artists = new ArrayList<>();
}

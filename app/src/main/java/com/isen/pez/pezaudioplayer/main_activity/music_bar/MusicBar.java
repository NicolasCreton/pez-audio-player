package com.isen.pez.pezaudioplayer.main_activity.music_bar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by cdupl on 11/4/2015.
 * Bar at the bottom of the screen to control music
 */
public class MusicBar extends View {

    public MusicBar(Context context) {
        super(context);
    }

    public MusicBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MusicBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MusicBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}

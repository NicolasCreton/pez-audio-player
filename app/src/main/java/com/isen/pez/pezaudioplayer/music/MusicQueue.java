package com.isen.pez.pezaudioplayer.music;

import android.util.Log;

import com.isen.pez.pezaudioplayer.model.Song;

import java.io.File;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by cdupl on 11/7/2015.
 */
public class MusicQueue {
    public LinkedList<Song> songs;
    public ListIterator<Song> iterator;

    public Song currentSong;
    public File currentFile;

    public MusicQueue() {
        songs = new LinkedList<>();
        iterator = songs.listIterator();
        currentSong = null;
    }

    public void addNext(Song song) {
        iterator.add(song);
    }

    public void addLast(Song song) {
        songs.addLast(song);
    }

    public void remove(Song song) {
        songs.remove(song);
    }

    public void replace(List<Song> songs_) {
        songs.clear();
        songs.addAll(songs_);
        iterator = songs.listIterator();
    }

    public void goBackToFirstSong() {
        this.iterator = songs.listIterator();
    }

    public void replace(Song firstSong, List<Song> songs_) {
        int nbAttempts = 0;
        int size = songs_.size();
        while (!songs_.get(0).equals(firstSong) && nbAttempts < size) {
            Song song = songs_.get(0);
            songs_.remove(0);
            songs_.add(song);
            nbAttempts++;
        }
        this.songs.clear();
        this.songs.addAll(songs_);
        this.iterator = this.songs.listIterator();

    }

    public Song next() {
        if (iterator.hasNext()) {
            currentSong = iterator.next();
        } else {
            currentSong = getCurrentSong();
        }
        return currentSong;

    }

    public Song previous() {
        //If song has started less than ~5 seconds ago~, go back twice

        if (iterator.hasPrevious()) {
            currentSong = iterator.previous();
        } else {
            currentSong = getCurrentSong();
        }
        return currentSong;
    }

    public Song get(int position) {
        return songs.get(position);
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public int size() {
        return songs.size();
    }

    public void remove(int position) {
        songs.remove(position);
        Log.i("MusicQueue", "Song Removed");
        this.iterator = songs.listIterator(iterator.nextIndex());
    }

    public void swap(int i, int i1) {
        if (songs.get(i).equals(currentSong)) {
            Collections.swap(songs,i,i1);
            iterator = songs.listIterator(i1+1);
            //Move iterator with it
        }
        else {
            Collections.swap(songs,i,i1);
            iterator = songs.listIterator(i+1);
        }

    }

    public Song goTo(Song song) {
        iterator = songs.listIterator();
        while (iterator.hasNext()) {
            Song foundSong = iterator.next();
            if (foundSong.equals(song)) {
                currentSong = foundSong;
                return foundSong;
            }
        }
        return null;
    }

    public void setCurrentFile(File file, boolean isFromSong) {
        currentFile = file;
        if (!isFromSong) {
            setCurrentSong(null);
        }
    }

    public File getCurrentFile() {
        return currentFile;
    }

    public void clear() {
        songs.clear();
        iterator = songs.listIterator();
        currentSong = null;
    }

    public void setCurrentSong(Song currentSong) {
        if (currentSong == null) {
            clear();
        }
        else {
            this.currentSong = currentSong;
        }
        
    }
}

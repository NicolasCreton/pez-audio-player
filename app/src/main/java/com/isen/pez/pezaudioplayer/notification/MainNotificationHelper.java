package com.isen.pez.pezaudioplayer.notification;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.support.v7.app.NotificationCompat;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MainActivity;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.music.MusicPlayerService;
import com.isen.pez.pezaudioplayer.utils.Constants;

import java.io.File;

/**
 * Helper class for showing and canceling main helper
 * notifications.
 * <p/>
 * This class makes heavy use of the {@link NotificationCompat.Builder} helper
 * class to create notifications in a backward-compatible way.
 */
public class MainNotificationHelper {
    /**
     * The unique identifier for this type of notification.
     */
    private static final String NOTIFICATION_TAG = "MainNotification";
    private static final int NOTIFICATION_ID = 1;

    public static void notify(final Context context) {
        Song currentSong = MusicPlayer.getQueue().getCurrentSong();
        File currentFile = MusicPlayer.getQueue().getCurrentFile();
        final Resources res = context.getResources();
        final Bitmap picture = BitmapFactory.decodeResource(res, R.drawable.example_picture);
        final String ticker = "Pez Audio Player";

        String songName;
        String artistName;
        String albumName;
        Bitmap image;

        if (currentSong != null) {
            songName = currentSong.mSongName;
            artistName = currentSong.mArtistName;
            albumName = currentSong.mAlbumName;
            image = currentSong.getImage();
            if (image == null) {
                image = BitmapFactory.decodeResource(PezApplication.getContext().getResources(), R.drawable.ic_empty_album);
            }
        } else if (currentFile != null) {
            MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
            mediaMetadataRetriever.setDataSource(currentFile.getPath());
            songName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
            albumName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            artistName = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            byte[] artWork;
            artWork = mediaMetadataRetriever.getEmbeddedPicture();
            if (artWork != null){
                image = BitmapFactory.decodeByteArray(artWork, 0, artWork.length);
            }
            else {
                image = BitmapFactory.decodeResource(PezApplication.getContext().getResources(), R.drawable.ic_empty_album);
            }
        }
        else {
            songName = "";
            artistName = "";
            albumName = "";
            image = BitmapFactory.decodeResource(PezApplication.getContext().getResources(), R.drawable.ic_empty_album);
        }

        final String title = songName;
        final String text = artistName+" - "+albumName;

        Intent notificationIntent = new Intent(context, MainActivity.class);

        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, 0);

        Intent previousIntent = new Intent(context, MusicPlayerService.class);
        previousIntent.setAction("Previous");

        PendingIntent ppreviousIntent = PendingIntent.getService(context, 0,
                previousIntent, 0);

        Intent playIntent = new Intent(context, MusicPlayerService.class);
        playIntent.setAction(Constants.MusicPlayer.TOGGLE_PLAY_PAUSE_ACTION);
        PendingIntent pplayIntent = PendingIntent.getService(context, 0,
                playIntent, 0);

        Intent nextIntent = new Intent(context, MusicPlayerService.class);
        nextIntent.setAction("Next");
        PendingIntent pnextIntent = PendingIntent.getService(context, 0,
                nextIntent, 0);


        NotificationCompat.Action action1 =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_media_previous,
                        "Previous", ppreviousIntent)
                        .build();

        NotificationCompat.Action action2 =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_media_next,
                        "Next", pnextIntent)
                        .build();

        NotificationCompat.Action action3 =
                new NotificationCompat.Action.Builder(android.R.drawable.ic_media_play,
                        "Play", pplayIntent)
                        .build();


        Notification notification = new NotificationCompat.Builder(context)
                .setContentTitle(title)
                .setTicker(ticker)
                .setContentText(text)
                .setSmallIcon(android.R.drawable.stat_sys_headset)
                .setLargeIcon(Bitmap.createScaledBitmap(image, 256, 256, false))

                .setStyle(new NotificationCompat.MediaStyle().setMediaSession(null))

                .addAction(android.R.drawable.ic_media_previous, "Previous", ppreviousIntent)

                .addAction(android.R.drawable.ic_media_play, "Play", pplayIntent)

                .addAction(android.R.drawable.ic_media_next, "Next", pnextIntent)

                .extend(new NotificationCompat.WearableExtender()
                        .setDisplayIntent(pendingIntent)
                        .setContentAction(2)
                        .setCustomSizePreset(Notification.WearableExtender.SIZE_MEDIUM)
                        .addAction(action1)
                        .addAction(action2)
                        .addAction(action3))
                .build();

        notify(context, notification);
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    private static void notify(final Context context, final Notification notification) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.notify(NOTIFICATION_TAG, NOTIFICATION_ID, notification);
        } else {
            nm.notify(NOTIFICATION_TAG.hashCode(), notification);
        }
    }

    /**
     * Cancels any notifications of this type previously shown using
     * {@link #notify(Context)}.
     */

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public static void cancel(final Context context) {
        final NotificationManager nm = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ECLAIR) {
            nm.cancel(NOTIFICATION_TAG, 0);
        } else {
            nm.cancel(NOTIFICATION_TAG.hashCode());
        }
    }
}

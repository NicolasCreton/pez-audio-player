package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistBiography;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestArtistBiographyQuery {
    @SerializedName("response")
    public EchoNestArtistBiographyResponse response;
}

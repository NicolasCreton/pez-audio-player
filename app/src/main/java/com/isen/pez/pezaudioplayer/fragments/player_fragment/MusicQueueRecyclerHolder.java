package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.asyncTask.ConverByteToBitmap;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsRecyclerHolder;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicPlayer;
import com.isen.pez.pezaudioplayer.utils.Constants;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/10/2015.
 */
public class MusicQueueRecyclerHolder extends SongsRecyclerHolder {

    public MusicQueueRecyclerHolder(View itemView, SongClickListener songClickListener) {
        super(itemView, songClickListener);
    }

    @Override
    public void onClick(View v) {
        getSongClickListener().playSongFromQueue(getSong());
    }

    public void bindSong(Song song) {
        super.bindSong(song);

        Log.i("QueueHolder", ""+MusicPlayer.getQueue().getCurrentSong());

        if (song.equals(MusicPlayer.getQueue().getCurrentSong())) {
            getTitle().setTypeface(null, Typeface.BOLD);
        }
        else {
            getTitle().setTypeface(null, Typeface.NORMAL);
        }
    }
}

package com.isen.pez.pezaudioplayer.main_activity.content;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v13.app.FragmentStatePagerAdapter;

import com.isen.pez.pezaudioplayer.fragments.albums_fragment.AlbumsFragment;
import com.isen.pez.pezaudioplayer.fragments.artists_fragment.ArtistsFragment;
import com.isen.pez.pezaudioplayer.fragments.file_explorer.FileExplorerFragment;
import com.isen.pez.pezaudioplayer.fragments.playlists_fragment.PlaylistsFragment;
import com.isen.pez.pezaudioplayer.interfaces.DirectoryClickListener;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsFragment;

import java.io.File;

/**
 * Created by cdupl on 11/6/2015.
 */
public class MyPagerAdapter extends FragmentStatePagerAdapter {

    private static int NB_TABS = 4;
    private final DirectoryClickListener mDirectoryClickListener;
    private final MusicActivity mMusicActivity;
    private FileExplorerFragment mFileExplorerFragment;

    public MyPagerAdapter(FragmentManager manager, MusicActivity mMusicActivity) {
        super(manager);
        mDirectoryClickListener = (DirectoryClickListener) mMusicActivity;
        this.mMusicActivity = mMusicActivity;
    }

    @Override
    public Fragment getItem(int i) {
        if (i == 0) {
            mFileExplorerFragment = FileExplorerFragment.newInstance(mDirectoryClickListener);
            return mFileExplorerFragment;
        }
        if(i==1){
            return ArtistsFragment.newInstance(mMusicActivity);
        }
        if(i==2){
            return AlbumsFragment.newInstance(mMusicActivity, null);
        }
        if (i == 3) {
            return SongsFragment.newInstance(mMusicActivity, null, null);
        }
        if (i == 4) {
            return PlaylistsFragment.newInstance(mMusicActivity);
        }
        return null;
    }

    public FileExplorerFragment getFileExplorerFragment() {
        return mFileExplorerFragment;
    }

    @Override
    public int getCount() {
        return NB_TABS;
    }

    public void setNewDirectory(File newDirectory) {
        mFileExplorerFragment.setDirectory(newDirectory);
    }
}

package com.isen.pez.pezaudioplayer.music;

import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.model.Song;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by cdupl on 11/7/2015.
 */
public interface IMusicPlayerController {
    void play(Song song, List<Song> songs);
    void play(String path) throws IOException;
    void play(Song song) throws IOException;
    void play(File file) throws IOException;
    void play(Album album);
    void play(Artist artist);
    void play();
    void pause();
    void togglePlayPause();

}

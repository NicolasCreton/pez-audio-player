package com.isen.pez.pezaudioplayer.main_activity.music_bar;

/**
 * Created by cdupl on 11/4/2015.
 */
public interface IMusicBarController {
    void bindMusicBar(MusicBar musicBar);
    void onBarClick();
    void onPlayPauseClick();
    void onNextClick();
    void onPreviousClick();
    void onShuffleClick();
    void onRepeatClick(); //Toggle between Repeat, Repeat One, None
}

package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistImage;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestImage {
    @SerializedName("url")
    public String url;
}

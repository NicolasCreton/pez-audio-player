package com.isen.pez.pezaudioplayer.fragments.playlists_fragment;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.interfaces.PlaylistClickListener;
import com.isen.pez.pezaudioplayer.model.Playlist;

/**
 * Created by ronan on 11/11/15.
 */
public class PlaylistsRecycleHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final TextView name;
    private Playlist playlist;
    private PlaylistClickListener mPlaylistClickListener;

    public PlaylistsRecycleHolder(View itemView, PlaylistClickListener playlistClickListener) {
        super(itemView);
        itemView.setOnClickListener(this);
        name = (TextView) itemView.findViewById(R.id.song_name);//tmp
        mPlaylistClickListener = playlistClickListener;
        //R.id
    }

    public void bindPlaylist(Playlist playlist) {
        this.playlist = playlist;
        name.setText(playlist.mPlaylistName);
    }

    @Override
    public void onClick(View v) {
        mPlaylistClickListener.onPlaylistClick(playlist);
    }
}

package com.isen.pez.pezaudioplayer;

import android.content.Intent;
import android.os.Bundle;

import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.utils.Constants;

import java.io.File;

/**
 * Created by cdupl on 11/8/2015.
 */
public class BroadcastManager {

    public static void sendPlaySongBroadcast(Song song) {
        if (song == null) {
            sendPlaySongBroadcast((String)null);
        }
        else {
            sendPlaySongBroadcast(song.getmPath());
        }
    }

    public static void sendPlaySongBroadcast() {
        sendPlaySongBroadcast((String)null);
    }

    public static void sendPlaySongBroadcast(File file) {
        sendPlaySongBroadcast(file.getPath());
    }

    public static void sendPlaySongBroadcast(String path) {
        //BroadCast
        Intent playBroadCast = new Intent(Constants.BroadcastConstants.PLAY);
        Bundle extraPath = new Bundle();
        extraPath.putString("pathFile", path);
        playBroadCast.putExtras(extraPath);
        PezApplication.getContext().sendBroadcast(playBroadCast);
    }
}

package com.isen.pez.pezaudioplayer.fragments.artists_fragment;

import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.asyncTask.ArtistArt;
import com.isen.pez.pezaudioplayer.asyncTask.ArtistArtEchoNest;
import com.isen.pez.pezaudioplayer.interfaces.ArtistClickListener;
import com.isen.pez.pezaudioplayer.model.Artist;


/**
 * Created by franc on 06/11/2015.
 */
public class ArtistsRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final TextView name;
    private Artist artist;
    private ArtistClickListener mArtistClickListener;
    private ImageView artistImage;


    public ArtistsRecyclerHolder(View itemView, ArtistClickListener artistClickListener) {
        super(itemView);
        name = (TextView) itemView.findViewById(R.id.artist_name);
        artistImage = (ImageView) itemView.findViewById(R.id.artist_image);
        mArtistClickListener = artistClickListener;
        itemView.setOnClickListener(this);


    }

    public void bindArtist(final Artist artist) {
        this.artist = artist;
        name.setText(artist.mArtistName);
        String url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(artist.mArtistName, "NotFound");
        if(url=="NotFound"){
            ArtistArt artistArtAsync = new ArtistArt(artist);
            artistArtAsync.execute();
        } else {
           AQuery androidQuery = new AQuery(PezApplication.getContext());
           androidQuery.id(artistImage).image(url, false, true,200,0);
        }
        //String imageUrl = artist.getImagePath();
        //Log.i(artist.mArtistName,""+imageUrl);
        //use on disk image
//        if (artist.isDoneWorkingOnImageAndHasImage()) {
//            PicassoHelper.with(PezApplication.getContext()).load(imageUrl).error(R.drawable.ic_empty_album).into(artistImage);
//        } else if (artist.getImagePath() == null && !artist.isWorkingOnImage()) { //get a brand new url if not already doing so
//            ArtistArt artistArtAsync = new ArtistArt(artist, artistImage);
//            artistArtAsync.execute();
//            //download this onto sdcard !
//            //DownloadFile dl = new DownloadFile(artist);
//            //dl.execute();
//        } else if (artist.isDoneWorkingOnImageAndHasNoImage()) {
//            artistImage.setImageResource(R.drawable.ic_empty_album);
//        }
    }


    @Override
    public void onClick(View v) {
        mArtistClickListener.onArtistClick(artist);
    }
}

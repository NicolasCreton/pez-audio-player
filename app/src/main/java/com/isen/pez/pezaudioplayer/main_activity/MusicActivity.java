package com.isen.pez.pezaudioplayer.main_activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.BroadcastManager;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.animations.Animations;
import com.isen.pez.pezaudioplayer.fragments.albums_fragment.AlbumsFragment;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsFragment;
import com.isen.pez.pezaudioplayer.fragments.toolbar_Fragment.ToolbarFragment;
import com.isen.pez.pezaudioplayer.interfaces.AlbumClickListener;
import com.isen.pez.pezaudioplayer.interfaces.ArtistClickListener;
import com.isen.pez.pezaudioplayer.interfaces.DirectoryClickListener;
import com.isen.pez.pezaudioplayer.interfaces.PlaylistClickListener;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.main_activity.music_bar.MusicController;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.model.Playlist;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicLibrary;
import com.isen.pez.pezaudioplayer.music.MusicPlayerService;
import com.isen.pez.pezaudioplayer.utils.Constants;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by cdupl on 11/4/2015.
 */
public abstract class MusicActivity extends AppCompatActivity  implements DirectoryClickListener, SongClickListener, ArtistClickListener,
        AlbumClickListener , PlaylistClickListener{
    private MusicController mMusicController = new MusicController(this);
    private MusicLibrary mMusicLibrary;

    private AlertDialog closeDialog = null;

    public MusicLibrary getMusicLibrary() {
        return mMusicLibrary;
    }

    public MusicActivity() {
        super();

    }

    public MusicController getMusicBarController() {
        return mMusicController;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMusicLibrary = new MusicLibrary();
    }

    @Override
    public abstract void onDirectoryClick(String dirPath);

    @Override
    public abstract void onDirectoryClick(File folder);

    @Override
    public void playSong(File file) {
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction(Constants.MusicPlayer.PLAY_FILE_PATH_ACTION);
        Bundle extras = new Bundle();
        extras.putString("path", file.getPath());
        startIntent.putExtras(extras);
        startService(startIntent);
//        //BroadCast
//        Intent playBroadCast = new Intent(Constants.BroadcastConstants.PLAY);
//        Bundle extraPath = new Bundle();
//        extraPath.putString("pathFile", file.getPath());
//        playBroadCast.putExtras(extraPath);
//        sendBroadcast(playBroadCast);
        //BroadcastManager.sendPlaySongBroadcast(file);

    }

    public void togglePlayPause() {
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction(Constants.MusicPlayer.TOGGLE_PLAY_PAUSE_ACTION);
        startService(startIntent);
    }

    public void nextSong(){
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction("Next");
        startService(startIntent);
    }
    public void previousSong(){
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction("Previous");
        startService(startIntent);
    }

    @Override
    public void playSong(Song song, ArrayList<Song> songs) {
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction(Constants.MusicPlayer.PLAY_LIST_SONGS_ACTION);
        Bundle extras = new Bundle();
        extras.putParcelable("songPath", song);
        extras.putParcelableArrayList("other_songs_path", songs);

        startIntent.putExtras(extras);
        startService(startIntent);
//        //BroadCast
//        Intent playBroadCast = new Intent(Constants.BroadcastConstants.PLAY);
//        Bundle extraPath = new Bundle();
//        extraPath.putString("pathFile", song.getmPath());
//        playBroadCast.putExtras(extraPath);
//        sendBroadcast(playBroadCast);
        //BroadcastManager.sendPlaySongBroadcast(song);
    }

    @Override
    public void onArtistClick(Artist artist) {
        Fragment albumFragment = AlbumsFragment.newInstance(this, artist.mArtistId);
        Log.d(artist.getImagePath(), "onArtistClick ");
        ToolbarFragment toolbarFragment = ToolbarFragment.newInstance(albumFragment, artist);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Animations.applyFragmentAnimation(transaction);
        transaction.replace(R.id.tabs_fragment_container, toolbarFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    @Override
    public void onAlbumClick(Album album) {
        Fragment songFragment = SongsFragment.newInstance(this, album.mAlbumId,null);
        Bitmap headerAlbum = album.getImage();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        ToolbarFragment toolbarFragment = ToolbarFragment.newInstance(songFragment, album);
        Animations.applyFragmentAnimation(transaction);
        transaction.replace(R.id.tabs_fragment_container, toolbarFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onPlaylistClick(Playlist playlist) {
        Fragment songFragment = SongsFragment.newInstance(this,null, playlist.mPlaylistId);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.tabs_fragment_container, songFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void askLeave() {
        AlertDialog closeDialog;
        closeDialog = makeCloseDialog();
        closeDialog.show();
    }

    private AlertDialog makeCloseDialog() {
        if (closeDialog != null) {
            return closeDialog;
        }

        closeDialog = new AlertDialog.Builder(this).create();
        closeDialog.setTitle("Leaving application");
        closeDialog.setMessage("Do you want to leave the application ?");
        closeDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //...
            }
        });

        closeDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Leave", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        /*closeDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getMessageFromResource(R.string.close_dialog_quit_choice), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                quit();
            }
        });*/
        return closeDialog;
    }

    @Override
    public void playSongFromQueue(Song song) {
        Intent startIntent = new Intent(MusicActivity.this, MusicPlayerService.class);
        startIntent.setAction(Constants.MusicPlayer.PLAY_SONG_IN_QUEUE);
        Bundle extras = new Bundle();
        extras.putParcelable("songPath", song);

        startIntent.putExtras(extras);
        startService(startIntent);
    }
}

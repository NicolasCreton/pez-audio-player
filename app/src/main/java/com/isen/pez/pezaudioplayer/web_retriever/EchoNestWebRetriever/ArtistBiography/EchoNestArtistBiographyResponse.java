package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistBiography;

import com.google.gson.annotations.SerializedName;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch.WebStatus;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestArtistBiographyResponse {
    @SerializedName("status")
    public WebStatus status;

    @SerializedName("biographies")
    public ArrayList<Biography> biographies = new ArrayList<>();

}

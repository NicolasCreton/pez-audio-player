package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistNews;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ArtistNews {
    @SerializedName("name")
    public String name;

    @SerializedName("url")
    public String url;

    @SerializedName("date_posted")
    public String datePosted;

    @SerializedName("summary")
    public String summary;

    @SerializedName("id")
    public String id;
}

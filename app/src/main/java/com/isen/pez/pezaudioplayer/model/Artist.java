package com.isen.pez.pezaudioplayer.model;

import android.database.Cursor;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.isen.pez.pezaudioplayer.PezApplication;

import java.io.File;
import java.util.HashMap;

public class Artist {

  private static HashMap<Long,String> ARTIST_IMAGES_HASHMAP = new HashMap<>();

  private static final String working = "working";
  private static final String finished = "finished";

  public long mArtistId;
  public String mArtistName;
  public int mAlbumNumber;
  public int mSongNumber;
  //public String mArtistImageUrl;
  //public String mArtistImagePath;

  public Artist(final long artistId, final String artistName, final int songNumber, final int albumNumber) {
    super();
    mArtistId = artistId;
    mArtistName = artistName;
    mSongNumber = songNumber;
    mAlbumNumber = albumNumber;
  }

  public static Artist artistFromCursor(Cursor mCursor){
    final long id = mCursor.getLong(0);
    final String artistName = mCursor.getString(1);
    final int albumCount = mCursor.getInt(2);
    final int songCount = mCursor.getInt(3);
    return new Artist(id, artistName, songCount, albumCount);
  }

  public void setWorkingOnImage(boolean b) {
    if (b) {
      setImagePath(working);
    }
    else {
      if (getImagePath().equals(working)) {
          setImagePath(null);
      }
    }

  }

  public boolean isDoneWorkingOnImageAndHasImage() {
    String path = getImagePath();
    return (path != null && !path.equals(working)) || finished.equals(path);
  }

  public boolean isDoneWorkingOnImageAndHasNoImage() {
    return finished.equals(getImagePath());
  }

  public void setImagePath(String url) {
    ARTIST_IMAGES_HASHMAP.put(this.mArtistId, url);
    PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).edit().putString(this.mArtistName, url).commit();
  }

  public String getImagePath() {
    String url = ARTIST_IMAGES_HASHMAP.get(this.mArtistId);
    if (url == null) {
      url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(this.mArtistName,null);
    }
    if (url != null) {
      setImagePath(url);
    }

    return url;
  }

  public boolean isWorkingOnImage() {
    return working.equals(getImagePath());
  }

  public void setDoneWorkingOnImage() {
    if (getImagePath() == null) {
      setImagePath(finished);
    }
  }
}

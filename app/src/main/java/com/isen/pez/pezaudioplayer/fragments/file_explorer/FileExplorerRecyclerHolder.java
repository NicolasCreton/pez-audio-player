package com.isen.pez.pezaudioplayer.fragments.file_explorer;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.isen.pez.pezaudioplayer.interfaces.DirectoryClickListener;
import com.isen.pez.pezaudioplayer.R;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by cdupl on 11/6/2015.
 */
public class FileExplorerRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private final DirectoryClickListener directoryClickListener;
    private File file;
    private TextView title;
    private ImageView image;
    private View container;

    public FileExplorerRecyclerHolder(View itemView, DirectoryClickListener directoryClickListener) {
        super(itemView);
        title = (TextView) itemView.findViewById(R.id.file_title);
        image = (ImageView) itemView.findViewById(R.id.file_image);
        this.directoryClickListener = directoryClickListener;
        container = itemView;
        itemView.setOnClickListener(this);
    }

    public void bindFile(File file) {
        this.file = file;

        title.setText(file.getName());
        if (file.isDirectory()) {
            image.setImageResource(R.drawable.ic_folder);
        } else if (file.getName().toLowerCase().endsWith(".mp3")
                || file.getName().toLowerCase().endsWith(".m4a")
                ||file.getName().toLowerCase().endsWith(".wav")
                ||file.getName().toLowerCase().endsWith(".flac")){
            image.setImageResource(R.drawable.ic_empty_album);
        } else {
            image.setImageResource(R.drawable.ic_file);
        }
    }

    @Override
    public void onClick(View v) {
        Log.i("FileHolder", file.getPath());
        if (file.isDirectory()) {
            directoryClickListener.onDirectoryClick(file);
        } else {
            directoryClickListener.playSong(file);
        }
    }
}

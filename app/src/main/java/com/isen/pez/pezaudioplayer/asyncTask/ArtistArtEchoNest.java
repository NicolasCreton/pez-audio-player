package com.isen.pez.pezaudioplayer.asyncTask;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.EchoNest;
import com.squareup.picasso.Callback;

import java.util.LinkedList;

/**
 * Created by franc on 08/11/2015.
 */
public class ArtistArtEchoNest extends AsyncTask<Void, Void, LinkedList<String>> {

    private static final int NB_ATTEMPTS = 3;

    ImageView artistArt;
    Artist mArtist;

    public ArtistArtEchoNest(Artist artist, ImageView artistArt) {
        this.artistArt = artistArt;
        mArtist = artist;
        mArtist.setWorkingOnImage(true);
    }

    @Override
    protected LinkedList<String> doInBackground(Void... params) {
        try {
            //Echonest
            String artistId = EchoNest.getArtistId(mArtist.mArtistName);
            LinkedList<String> urls = EchoNest.getArtistImagePath(artistId);
            return urls;
                        /*Log.d("EchoNestURL", imageUrl);
                            //if still empty, google Images
                        if(urls == null || urls.size() == 0) {
                            if (!Patterns.WEB_URL.matcher(imageUrl).matches()) { //Auto redirect to google image, add ! to use ehconest
                                URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + mArtist.mArtistName + "+musician&rsz=1&tbs=itp:photo,isz:m&tbm=isch");
                                URLConnection connection = url.openConnection();
                                String line;
                                StringBuilder builder = new StringBuilder();
                                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                                while ((line = reader.readLine()) != null) {
                                    builder.append(line);
                                }

                                JSONObject json = new JSONObject(builder.toString());
                                JSONObject json2 = json.getJSONObject("responseData");
                                JSONArray json3 = json2.getJSONArray("results");
                                JSONObject json4 = json3.getJSONObject(0);
                                imageUrl = json4.getString("unescapedUrl");
                            }
            }*/
            } catch (Exception e) {
            e.printStackTrace();
            }
        return null;

    }

    private void load(final LinkedList<String> urls, final int i, final int nbAttempts) {
        if (i < urls.size()) {
            final String url = urls.get(i);
            if (url.startsWith("http://userserve-ak.last.fm/") && i + 1 < urls.size()) {
                load(urls,i+1, nbAttempts);
            }
            else {
                PicassoHelper.with(PezApplication.getContext()).load(url).placeholder(R.drawable.ic_empty_album).error(R.drawable.ic_empty_album).into(artistArt, new Callback() {
                    @Override
                    public void onSuccess() {
                        mArtist.setImagePath(url);
                    }

                    @Override
                    public void onError() {
                        Log.i("Picasso", "onError " + url);
                        if (i < urls.size() && nbAttempts < NB_ATTEMPTS) {
                            load(urls, i + 1, nbAttempts + 1);
                        } else {
                            mArtist.setWorkingOnImage(false);
                            mArtist.setDoneWorkingOnImage();
                            //New AsyncTask
                        }
                    }
                });
            }

        }

    }

    @Override
    protected void onPostExecute(LinkedList<String> urls) {
        load(urls, 0, 0);
    }
}

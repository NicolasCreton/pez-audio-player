package com.isen.pez.pezaudioplayer.main_activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;

import com.isen.pez.pezaudioplayer.R;

/**
 * Created by nicolas on 12/11/2015.
 */
public class AboutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_page);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}

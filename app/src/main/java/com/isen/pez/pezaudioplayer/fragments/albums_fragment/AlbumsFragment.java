package com.isen.pez.pezaudioplayer.fragments.albums_fragment;


import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.music.MusicLibrary;

/**
 * A simple {@link Fragment} subclass.
 */
public class AlbumsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private MusicActivity mActivity;
    private MusicLibrary mMusicLibrary;
    private AlbumsCursorAdapter mAdapter;
    private Long mArtistId = null;

    public AlbumsFragment() {
        // Required empty public constructor
    }

    public static AlbumsFragment newInstance(MusicActivity activity, Long artistId) {
        Log.i("newAlbumsFrag", ""+artistId);
        AlbumsFragment fragment = new AlbumsFragment();
        fragment.mArtistId = artistId;
        fragment.mActivity = activity;
        fragment.mMusicLibrary = activity.getMusicLibrary();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.albums_fragment, container, false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(PezApplication.getContext(), 2));
        setupRecyclerView(mRecyclerView);
        getLoaderManager().restartLoader(0, null, this);
        return mRecyclerView;
    }

    public void setupRecyclerView(RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
        mAdapter = null;
        setAdapter(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;
        if (mArtistId == null) {
            cursorLoader = mMusicLibrary.initAlbumCursor();
        } else {
            cursorLoader = mMusicLibrary.initArtistAlbumCursor(mArtistId);
        }
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i("onLoadFinished",""+data);
        setAdapter(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setAdapter(Cursor data) {
        if (mAdapter != null) {
            mAdapter.changeCursor(data);
        } else {
            mAdapter = new AlbumsCursorAdapter(mActivity.getApplicationContext(), data, mActivity);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}

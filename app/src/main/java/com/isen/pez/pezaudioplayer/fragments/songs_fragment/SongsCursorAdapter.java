package com.isen.pez.pezaudioplayer.fragments.songs_fragment;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.utils.CursorRecyclerViewAdapter;

/**
 * Created by cdupl on 11/6/2015.
 */
public class SongsCursorAdapter extends CursorRecyclerViewAdapter<SongsRecyclerHolder> {


    private SongClickListener mSongClickListener;

    public SongsCursorAdapter(Context context, Cursor cursor, SongClickListener songClickListener) {
        super(context, cursor);
        mSongClickListener = songClickListener;
    }

    @Override
    public void onBindViewHolder(SongsRecyclerHolder viewHolder, Cursor cursor) {
        viewHolder.bindSong(cursor);
    }

    @Override
    public SongsRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_song, parent, false);
        return new SongsRecyclerHolder(view, mSongClickListener);
    }
}

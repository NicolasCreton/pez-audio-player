package com.isen.pez.pezaudioplayer.asyncTask;

import android.os.AsyncTask;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.BitmapAjaxCallback;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;

/**
 * Created by cdupl on 11/11/2015.
 */
public class FileSystemSaver extends AsyncTask<ImageView, Void, Void> {

    private final String mUrl;

    private static AQuery aQuery = initAndroidQuery();

    private static AQuery initAndroidQuery() {
        /* Settings of Image */
        //set the max number of concurrent network connections, default is 4
        AjaxCallback.setNetworkLimit(8);

        //set the max number of icons (image width <= 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setIconCacheLimit(50);

        //set the max number of images (image width > 50) to be cached in memory, default is 20
        BitmapAjaxCallback.setCacheLimit(50);

        return new AQuery(PezApplication.getContext());

    }

    public FileSystemSaver(String url) {
        this.mUrl = url;

    }

    @Override
    protected Void doInBackground(ImageView... params) {
        for (ImageView image : params) {
            aQuery.id(image).image(mUrl, true, true, 200, R.drawable.ic_empty_album, null, AQuery.FADE_IN);
        }

        return null;
    }
}



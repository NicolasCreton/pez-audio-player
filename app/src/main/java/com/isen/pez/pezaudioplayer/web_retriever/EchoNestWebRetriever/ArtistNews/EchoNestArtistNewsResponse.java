package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistNews;

import com.google.gson.annotations.SerializedName;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch.WebStatus;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestArtistNewsResponse {
    @SerializedName("status")
    public WebStatus status;

    @SerializedName("news")
    public ArrayList<ArtistNews> news = new ArrayList<>();
}

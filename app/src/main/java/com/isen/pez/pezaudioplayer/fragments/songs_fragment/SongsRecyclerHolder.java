package com.isen.pez.pezaudioplayer.fragments.songs_fragment;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.asyncTask.AlbumArt;
import com.isen.pez.pezaudioplayer.asyncTask.ConverByteToBitmap;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.model.Song;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/6/2015.
 */
public class SongsRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final TextView title;
    private final ImageView cover;
    private final SongClickListener mSongClickListener;
    private Song song;
    private View container;
    private Cursor mCursor;

    public SongsRecyclerHolder(View itemView, SongClickListener songClickListener) {
        super(itemView);
        mSongClickListener = songClickListener;
        container = itemView;
        title = (TextView) itemView.findViewById(R.id.song_name);
        cover = (ImageView) itemView.findViewById(R.id.coverImageSongs);
    }

    public void bindSong(Song song) {
        this.song = song;
        /**
         * Dans song, toutes les infos nécessaires :3 gg François
         */

        title.setText(song.mSongName);
        ConverByteToBitmap c = new ConverByteToBitmap(cover);
        c.execute(song);

        /*if (getCoverFromPath(song) != null) {
            cover.setImageBitmap(getCoverFromPath(song));
        } else{
            String url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(song.mArtistName + song.mAlbumName, "NotFound");
            if(url=="NotFound"){
                PicassoHelper.with(PezApplication.getContext()).load(R.drawable.ic_empty_album).into(cover);
            } else {
                AQuery androidQuery = new AQuery(PezApplication.getContext());
                androidQuery.id(cover).image(url, false, true,200,0);
            }
        }*/


        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        mSongClickListener.playSong(song, toArrayList(mCursor));
    }

    public static ArrayList<Song> toArrayList(Cursor mCursor) {
        ArrayList<Song> mArrayList = new ArrayList<>();
        for(mCursor.moveToFirst(); !mCursor.isAfterLast(); mCursor.moveToNext()) {
            // The Cursor is now set to the right position
            mArrayList.add(Song.songFromCursor(mCursor));
        }
        return mArrayList;
    }

    public void bindSong(Cursor cursor) {
        bindSong(Song.songFromCursor(cursor));
        this.mCursor = cursor;
    }

    public Bitmap getCoverFromPath(Song song){
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(song.getmPath());
        byte[] artWork;
        artWork = mediaMetadataRetriever.getEmbeddedPicture();
        if (artWork != null){
            Bitmap cover = BitmapFactory.decodeByteArray(artWork, 0, artWork.length);
            return cover;
        }
        return null;
    }

    public Song getSong() {
        return song;
    }

    public SongClickListener getSongClickListener() {
        return mSongClickListener;
    }

    public TextView getTitle() {
        return title;
    }
}

package com.isen.pez.pezaudioplayer.music;

/**
 * Created by cdupl on 11/6/2015.
 */
public enum MusicPlayerState {
    IDLE("Idle"),
    INITIALIZED("Initialized"),
    PREPARED("Prepared"),
    PREPARING("Preparing"),
    STOPPED("Stopped"),
    STARTED("Started"),
    PLAYBACK_COMPLETED("Playback Completed"),
    PAUSED("Paused"),
    END("End"),
    ERROR("Error");

    MusicPlayerState(String title) {
        this.title = title;
    }

    private String title;

    public String get() {
        return title;
    }
}

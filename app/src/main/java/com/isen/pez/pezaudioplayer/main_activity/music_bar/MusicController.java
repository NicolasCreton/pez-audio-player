package com.isen.pez.pezaudioplayer.main_activity.music_bar;

import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;

/**
 * Created by cdupl on 11/4/2015.
 */
public class MusicController implements IMusicBarController {

    private final MusicActivity mMusicActivity;

    public MusicController(MusicActivity musicActivity) {
        this.mMusicActivity = musicActivity;
    }


    @Override
    public void bindMusicBar(MusicBar musicBar) {

    }

    @Override
    public void onBarClick() {

    }

    @Override
    public void onPlayPauseClick() {
        mMusicActivity.togglePlayPause();
    }

    @Override
    public void onNextClick() { mMusicActivity.nextSong();

    }

    @Override
    public void onPreviousClick() { mMusicActivity.previousSong();

    }

    @Override
    public void onShuffleClick() {

    }

    @Override
    public void onRepeatClick() {

    }
}

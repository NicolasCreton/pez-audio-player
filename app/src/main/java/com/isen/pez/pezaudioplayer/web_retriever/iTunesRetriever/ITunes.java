package com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever.AlbumLookUp.ITunesLookUpQuery;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ITunes {

    //http://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html#overview

    public static final String iTunesUrl = "https://itunes.apple.com/";
    public static final String iTunesSearchUrl = iTunesUrl+"search?term=";
    public static final String iTunesAlbumLookUp = iTunesUrl+"lookup?entity=album&id=";

    public static String getArtistId(String artist) {
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(iTunesSearchUrl + artist.replaceAll(" ","+"));
            //If success
            if (connection != null) {
                //Build our artists list
                ITunesSearchQuery response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), ITunesSearchQuery.class);
                if (response.results != null && response.results.size() > 0) {
                    return String.valueOf(response.results.get(0).artistId);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static List<ITunesAlbum> getAlbums(String artistId) {
        try {
            final HttpURLConnection connection = getHTTPUrlConnection(iTunesAlbumLookUp + artistId);
            //If success
            if (connection != null) {
                //Build our artists list

                ITunesLookUpQuery response = new Gson().fromJson(new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8")), ITunesLookUpQuery.class);
                if (response.albums != null && response.albums.size() > 1) {
                    response.albums.remove(0); //Artist
                    return response.albums;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ITunesAlbum getAlbum(String artistId, String albumName) {
        List<ITunesAlbum> albums = getAlbums(artistId);
        for (ITunesAlbum album : albums) {
            if (albumName == album.albumName)
            {
                return album;
            }
        }
        return null;
    }


    private static HttpURLConnection getHTTPUrlConnection(String url) throws Exception {
        Log.i("Echo Nest", url);
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("Content-Type", "application/json");

        final int responseCode = connection.getResponseCode();
        if (responseCode == 200) {
            Log.i("iTunes", "Success");
            return connection;
        }
        return null;
    }

    /**
     * Debug function to print the JSON in adb.
     *
     * @param connection
     */
    private static String printJSON(final HttpURLConnection connection) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;

            while ((line = br.readLine()) != null) {
                sb.append(line + "\n");
            }
            br.close();
            Log.i("printJSON", sb.toString());
            return sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

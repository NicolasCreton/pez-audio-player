package com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever.AlbumLookUp;

import com.google.gson.annotations.SerializedName;
import com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever.ITunesAlbum;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ITunesLookUpQuery {
    @SerializedName("results")
    public ArrayList<ITunesAlbum> albums;
}

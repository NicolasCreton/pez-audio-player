package com.isen.pez.pezaudioplayer.fragments.player_fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.fragments.player_fragment.swipe_drag_interfaces.SimpleItemTouchHelperCallback;
import com.isen.pez.pezaudioplayer.interfaces.SongClickListener;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;

/**
 * Created by cdupl on 11/10/2015.
 */
public class QueueFragment extends Fragment {
    private RecyclerView mRecyclerView;
    private PlayerFragment mPlayerFragment;
    private MusicQueueAdapter mAdapter;
    private SongClickListener mSongClickListener;

    public static QueueFragment newInstance(SongClickListener mSongClickListener, PlayerFragment playerFragment) {
        QueueFragment queueFragment = new QueueFragment();
        queueFragment.mSongClickListener = mSongClickListener;
        queueFragment.mPlayerFragment = playerFragment;
        return queueFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.queue_fragment, container, false);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new MusicQueueAdapter(mSongClickListener);
        mRecyclerView.setAdapter(mAdapter);
        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(mRecyclerView);
        return mRecyclerView;
    }

    @Override
    public void onResume() {
        super.onResume();
        notifyDataSetChanged();
    }

    public void notifyDataSetChanged() {
        if (mAdapter != null)        mAdapter.notifyDataSetChanged();
    }
}

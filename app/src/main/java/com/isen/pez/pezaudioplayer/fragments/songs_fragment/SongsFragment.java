package com.isen.pez.pezaudioplayer.fragments.songs_fragment;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.model.Song;
import com.isen.pez.pezaudioplayer.music.MusicLibrary;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/6/2015.
 */
public class SongsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private MusicActivity mActivity;
    private MusicLibrary mMusicLibrary;
    private SongsCursorAdapter mAdapter;
    private Long mAlbumId = null;
    private Long mPlaylistId = null;

    public static SongsFragment newInstance(MusicActivity activity, Long albumId, Long playlistId) {
        SongsFragment fragment = new SongsFragment();
        fragment.mActivity = activity;
        fragment.mMusicLibrary = activity.getMusicLibrary();
        fragment.mAlbumId = albumId;
        fragment.mPlaylistId = playlistId;
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.songs_fragment, container, false);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        setupRecyclerView(mRecyclerView);
        mAdapter = null;
        getLoaderManager().restartLoader(0, null, this);
        return mRecyclerView;
    }

    public void setupRecyclerView(RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
        mAdapter = null;
        setAdapter(null);
        //mRecyclerView.setAdapter(new SongsCursorAdapter(mActivity.getApplicationContext(), null));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final CursorLoader cursorLoader;
        if(mAlbumId==null && mPlaylistId==null){
            cursorLoader = mMusicLibrary.initSongsCursor();
        } else if (mAlbumId==null){
            cursorLoader = mMusicLibrary.initPlaylistSongCursor(mPlaylistId);
        } else {
            cursorLoader = mMusicLibrary.initAlbumSongCursor(mAlbumId);
        }

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setAdapter(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setAdapter(Cursor data) {
        if (mAdapter != null) {
            mAdapter.changeCursor(data);
        }
        else {
            mAdapter = new SongsCursorAdapter(mActivity.getApplicationContext(), data, mActivity);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}

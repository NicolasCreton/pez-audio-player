package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class WebArtist {
    @SerializedName("name")
    public String name;

    @SerializedName("id")
    public String id;

}

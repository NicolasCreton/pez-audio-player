package com.isen.pez.pezaudioplayer.interfaces;

import com.isen.pez.pezaudioplayer.model.Playlist;

/**
 * Created by ronan on 11/11/15.
 */
public interface PlaylistClickListener {
    void onPlaylistClick(Playlist playlist);
}


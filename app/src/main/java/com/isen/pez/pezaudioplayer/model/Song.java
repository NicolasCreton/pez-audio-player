package com.isen.pez.pezaudioplayer.model;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.PreferenceManager;

import com.isen.pez.pezaudioplayer.PezApplication;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Song implements Parcelable {

    public static HashMap<String, Song> songs = new HashMap<>();

    public static HashMap<Long, Bitmap> albumImages = new HashMap<>();

    public static HashMap<Long, Bitmap> songImages = new HashMap<>();

    public static HashMap<Long, String> albumImageUrls = new HashMap<>();

    public static HashMap<Long, String> songImageUrls = new HashMap<>();


    public long mSongId;
    public String mSongName;
    public String mArtistName;
    public String mAlbumName;
    public String mDuration;
    public String mPath;
    private String imagePath;
    private Long mAlbumId;

    public static Song getInstance(final long songId, final String songName, final String artistName, final String albumName, final String path, final String duration, final Long albumId) {
        Song song = songs.get(path);
        if (song == null) {
            song = new Song(songId, songName, artistName, albumName, path, duration, albumId);
            songs.put(path, song);
        }

        return song;
    }

    public static Song findInstance(String path) {
        Song song = songs.get(path);
        if (song == null) {
            Iterator it = songs.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if((new File((String)pair.getKey())).getName().equals((new File(path)).getName())) {
                    return (Song)pair.getValue();
                }
                it.remove(); // avoids a ConcurrentModificationException
            }
        }
        return song;
    }

    private Song(final long songId, final String songName, final String artistName, final String albumName, final String path, final String duration, final Long albumId) {
        mSongId = songId;
        mSongName = songName;
        mArtistName = artistName;
        mAlbumName = albumName;
        mPath = path;
        mDuration = duration;
        mAlbumId = albumId;
    }

    @Override
    public boolean equals(Object o) {
        try {
            Song song = (Song) o;
            return mSongId == song.mSongId;
        } catch (Exception e) {

        }
        return false;
    }

    public static Song songFromCursor(Cursor mCursor) {
        final long id = mCursor.getLong(0);
        final String songName = mCursor.getString(1);
        final String artist = mCursor.getString(2);
        final String album = mCursor.getString(3);
        final String path = mCursor.getString(4);
        final String duration = mCursor.getString(5);

        final Long albumId;
        if (mCursor.getColumnCount() == 7) {
            albumId = mCursor.getLong(6);
        } else {
            albumId = null;
        }
        return Song.getInstance(id, songName, artist, album, path, duration, albumId);
    }


    public String getmPath() {
        return mPath;
    }

    public void setImage(Bitmap bitmap) {
        if (mAlbumId != null) {
            albumImages.put(mAlbumId, bitmap);
        } else {
            songImages.put(mSongId, bitmap);
        }

    }

    public Bitmap getImage() {
        Bitmap b = null;
        if (mAlbumId != null) {
            b = albumImages.get(mAlbumId);
        }
        if (b == null) {
            b = songImages.get(mSongId);
        }
        return b;
    }

    public void setImageUrl(String url) {
        if (mAlbumId != null) {
            albumImageUrls.put(mAlbumId, url);
        } else {
            songImageUrls.put(mSongId, url);
        }

    }

    public String getImageUrl() {
        String url = null;
        if (mAlbumId != null) {
            url = albumImageUrls.get(mAlbumId);
        } else {
            url = songImageUrls.get(mSongId);
        }
        if (url == null) {
            url = PreferenceManager.getDefaultSharedPreferences(PezApplication.getContext()).getString(mArtistName + mAlbumName, null);
        }
        if (url != null) {
            setImageUrl(url);
        }
        return url;
    }

    protected Song(Parcel in) {
        mSongId = in.readLong();
        mSongName = in.readString();
        mArtistName = in.readString();
        mAlbumName = in.readString();
        mDuration = in.readString();
        mPath = in.readString();
        imagePath = in.readString();
        mAlbumId = in.readByte() == 0x00 ? null : in.readLong();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mSongId);
        dest.writeString(mSongName);
        dest.writeString(mArtistName);
        dest.writeString(mAlbumName);
        dest.writeString(mDuration);
        dest.writeString(mPath);
        dest.writeString(imagePath);
        if (mAlbumId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(mAlbumId);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel in) {
            return new Song(in);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };
}
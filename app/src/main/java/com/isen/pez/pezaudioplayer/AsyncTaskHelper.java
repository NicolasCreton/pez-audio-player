package com.isen.pez.pezaudioplayer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by cdupl on 11/11/2015.
 */
public class AsyncTaskHelper {
    public static ThreadPoolExecutor threadPoolExecutor = initThreadPoolExecutor();

    private static ThreadPoolExecutor initThreadPoolExecutor() {
        int corePoolSize = 60;
        int maximumPoolSize = 80;
        int keepAliveTime = 10;

        BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(maximumPoolSize);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, TimeUnit.SECONDS, workQueue);
        return threadPoolExecutor;
    }
}

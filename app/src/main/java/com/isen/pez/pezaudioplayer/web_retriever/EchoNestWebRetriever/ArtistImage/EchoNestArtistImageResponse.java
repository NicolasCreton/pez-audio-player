package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistImage;

import com.google.gson.annotations.SerializedName;
import com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch.WebStatus;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestArtistImageResponse {
    @SerializedName("status")
    public WebStatus status;

    @SerializedName("start")
    public String start;

    @SerializedName("total")
    public String total;

    @SerializedName("images")
    public ArrayList<EchoNestImage> images = new ArrayList<>();

}

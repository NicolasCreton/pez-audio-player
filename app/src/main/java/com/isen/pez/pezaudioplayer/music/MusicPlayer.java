package com.isen.pez.pezaudioplayer.music;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.isen.pez.pezaudioplayer.BroadcastManager;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.isen.pez.pezaudioplayer.model.Song;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by cdupl on 11/6/2015.
 */
public class MusicPlayer {

    private static StateMediaPlayer mMediaPlayer = initMediaPlayer();
    private static AudioManager.OnAudioFocusChangeListener mFocusChangeListener;
    private static SurfaceView mSurfaceView;
    private static SurfaceHolderCallback mSurfaceHolderCallback;

    private static MusicQueue mQueue = new MusicQueue();


    private static StateMediaPlayer initMediaPlayer() {
        mMediaPlayer = new StateMediaPlayer();
        mFocusChangeListener = new MusicPlayerFocusChangeListener(mMediaPlayer);
        mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        initMediaPlayerListeners();
        return mMediaPlayer;
    }

    private static void initMediaPlayerListeners() {
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                mMediaPlayer.onPrepared(mp);
                try {
                    play();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                mMediaPlayer.onCompletion(mp);
                try {
                    next();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //New song or stop
            }
        });

        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {

            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                boolean result = mMediaPlayer.onError(mp, what, extra);
                //Well... there's a problem here. Maybe we should replace the media player ?
                return result;
            }
        });
    }

    private static boolean requestFocus() {
        AudioManager am = (AudioManager) PezApplication.getContext().getSystemService(Context.AUDIO_SERVICE);
        // Request audio focus for playback
        int result = am.requestAudioFocus(mFocusChangeListener,
                // Use the music stream.
                AudioManager.STREAM_MUSIC,
                // Request permanent focus.
                AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            return true;
        }
        return false;
    }



    public static void playNext(Song song) {
        mQueue.addNext(song);
    }

    public static void playLast(Song song) {
        mQueue.addLast(song);
    }

    public static void removeFromQueue(Song song) {
        mQueue.remove(song);
    }

    public static void next() throws IOException {
        Song song = mQueue.next();
        Log.i("next()", "" + song);
        if (song != null) {
            play(song);
        }
        else {
            stopRunningAction();
        }
    }

    public static void previous() throws IOException {
        Song song = mQueue.previous();
        play(song);
    }

    public static void play(Song song) throws IOException {
        mQueue.setCurrentSong(song);
        play(song.mPath, true);
    }

    public static void play(String path, boolean isFromSong) throws IOException {
        play(new File(path), isFromSong);
    }

    public static void play(String path) throws IOException {
        play(path, false);
    }

    private static void stopRunningAction() {
        if (mMediaPlayer.isState_Or(MusicPlayerState.STARTED, MusicPlayerState.PAUSED, MusicPlayerState.STOPPED, MusicPlayerState.PLAYBACK_COMPLETED, MusicPlayerState.PREPARED)) {
            mMediaPlayer.stop();
        }
        mMediaPlayer.reset();
    }

    public static void play(File file) throws IOException {
        play(file, false);
    }

    public static void play(File file, boolean isFromSong) throws IOException {
        Log.i("MusicPlayer", "" + mMediaPlayer.getState());
        stopRunningAction();
        mQueue.setCurrentFile(file, isFromSong);

        mMediaPlayer.setDataSource(PezApplication.getContext(), Uri.fromFile(file));
        mMediaPlayer.prepareAsync();
    }

    public static void play(Song song, List<Song> songs) throws IOException {
        stopRunningAction();
        mQueue.replace(song, songs);
        next();
    }

    public static void play(Album album) {

    }

    public static void play(Artist artist) {

    }

    public static boolean play() throws IOException {
        if (mMediaPlayer.isState(MusicPlayerState.IDLE)) {
            mQueue.goBackToFirstSong();
            next();
        }
        if (mMediaPlayer.isState_Or(MusicPlayerState.PREPARED, MusicPlayerState.PAUSED, MusicPlayerState.PLAYBACK_COMPLETED) && requestFocus()) {
            mMediaPlayer.start();
            return true;
        }
        return false;
    }

    public static boolean pause() {
        if (mMediaPlayer.isState(MusicPlayerState.STARTED)) {
            mMediaPlayer.pause();
            return true;
        }
        return false;
    }

    public static void togglePlayPause() {
        if (!pause()) {
            try {
                play();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//        if (mMediaPlayer.isState(MusicPlayerState.STARTED)) {
//            mMediaPlayer.pause();
//        } else if (mMediaPlayer.isState(MusicPlayerState.PAUSED)) {
//            mMediaPlayer.start();
//        }
    }

    public static void setSurfaceView(SurfaceView surfaceView) {
        mSurfaceView = surfaceView;
        SurfaceHolder mHolder = mSurfaceView.getHolder();
        mSurfaceHolderCallback = new SurfaceHolderCallback();
        mHolder.addCallback(mSurfaceHolderCallback);

    }


    public static StateMediaPlayer getMediaPlayer() {
        return mMediaPlayer;
    }

    public static SurfaceView getSurfaceView() {
        return mSurfaceView;
    }

    public static MusicQueue getQueue() {
        return mQueue;
    }

    public static void playSongInQueue(Song song) throws IOException {
        play(mQueue.goTo(song));
    }
}

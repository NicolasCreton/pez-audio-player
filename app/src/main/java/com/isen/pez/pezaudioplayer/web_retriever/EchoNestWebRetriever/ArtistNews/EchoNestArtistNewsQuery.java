package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistNews;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class EchoNestArtistNewsQuery {

    @SerializedName("response")
    public EchoNestArtistNewsResponse response;
}

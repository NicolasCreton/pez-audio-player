package com.isen.pez.pezaudioplayer.fragments.artists_fragment;


import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.music.MusicLibrary;

/**
 * A simple {@link Fragment} subclass.
 */
public class ArtistsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private MusicActivity mActivity;
    private MusicLibrary mMusicLibrary;
    private ArtistsCursorAdapter mAdapter;

    public ArtistsFragment() {
        // Required empty public constructor
    }

    public static ArtistsFragment newInstance(MusicActivity activity) {
        ArtistsFragment fragment = new ArtistsFragment();
        fragment.mActivity = activity;
        fragment.mMusicLibrary = activity.getMusicLibrary();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.artists_fragment, container, false);
        mRecyclerView.setLayoutManager(new GridLayoutManager(PezApplication.getContext(), 2));
        setupRecyclerView(mRecyclerView);
        mAdapter = null;
        getLoaderManager().restartLoader(0, null, this);
        return mRecyclerView;
    }

    public void setupRecyclerView(RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
        mAdapter = null;
        setAdapter(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        final CursorLoader cursorLoader;
        cursorLoader = mMusicLibrary.initArtistCursor();
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        setAdapter(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setAdapter(Cursor data) {
        if (mAdapter != null) {
            mAdapter.changeCursor(data);
        }
        else {
            mAdapter = new ArtistsCursorAdapter(mActivity.getApplicationContext(), data, mActivity);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}

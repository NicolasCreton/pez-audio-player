package com.isen.pez.pezaudioplayer.fragments.toolbar_Fragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.fragments.albums_fragment.AlbumsFragment;
import com.isen.pez.pezaudioplayer.fragments.artists_fragment.ArtistsFragment;
import com.isen.pez.pezaudioplayer.fragments.songs_fragment.SongsFragment;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.model.Album;
import com.isen.pez.pezaudioplayer.model.Artist;

/**
 * Created by nicolas on 11/11/2015.
 */
public class ToolbarFragment extends Fragment {

    private View rootView;
    Fragment innerFragment;
    Bitmap header;
    String url;
    String title;
    private RecyclerView mNewsRecyclerView;

    public static ToolbarFragment newInstance(Fragment innerFragment, Object object) {
        ToolbarFragment fragment = new ToolbarFragment();
        fragment.innerFragment = innerFragment;
        if (object instanceof Artist){
            fragment.url = ((Artist) object).getImagePath();
            fragment.header = null;
            fragment.title = ((Artist) object).mArtistName;
        }else if (object instanceof Album){
            fragment.header = ((Album) object).getImage();
            fragment.title = ((Album) object).mAlbumName;
            fragment.url = ((Album)object).getImageUrl();
        }
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.toolbar_fragment, container, false);
        Toolbar tbar = (Toolbar)rootView.findViewById(R.id.toolbarFromFrag);
        //mNewsRecyclerView = (RecyclerView)rootView.findViewById(R.id.newsRecyclerView);
        ((AppCompatActivity)getActivity()).setSupportActionBar(tbar);
        tbar.setTitle(title);
        ActionBar bar = ((AppCompatActivity)getActivity()).getSupportActionBar();
        bar.setDisplayHomeAsUpEnabled(true);

        addFragIn(innerFragment);

        ImageView headerParallax = (ImageView)rootView.findViewById(R.id.headerParallax);
        if (url != null){
            AQuery androidQuery = new AQuery(PezApplication.getContext());
            androidQuery.id(headerParallax).image(url, false, true, 200, 0);
        }else {
            headerParallax.setImageBitmap(header);
        }
        return rootView;
    }

    public void addFragIn(Fragment fragment){
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        Fragment taggedFragment = getChildFragmentManager().findFragmentByTag("innerFrag");
        if (taggedFragment != null){
            transaction.addToBackStack(null);
            transaction.remove(taggedFragment);
        }
        transaction.add(R.id.innerFrag, fragment, "innerFrag").commit();
    }
}

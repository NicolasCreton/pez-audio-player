package com.isen.pez.pezaudioplayer.web_retriever.iTunesRetriever;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class ITunesAlbum {

    @SerializedName("artistName")
    public String artistName;

    @SerializedName("collectionName")
    public String albumName;

    @SerializedName("collectionId")
    public int collectionId;

    @SerializedName("artworkUrl60")
    public String albumCover60;

    @SerializedName("artworkUrl100")
    public String albumCover100;

    @SerializedName("releaseDate")
    public String releaseDate;

    public String getSizedAlbumCover(int width) {
        if (albumCover100 != null) {
            return albumCover100.replaceAll("100",String.valueOf(width));
        }
        return null;
    }
}

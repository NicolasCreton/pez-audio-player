package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class WebStatus {
    @SerializedName("version")
    public String version;

    @SerializedName("code")
    public String code;

    @SerializedName("message")
    public String message;
}

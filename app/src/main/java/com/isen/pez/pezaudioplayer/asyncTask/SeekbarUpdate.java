package com.isen.pez.pezaudioplayer.asyncTask;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.SeekBar;

import com.isen.pez.pezaudioplayer.music.MusicPlayerState;
import com.isen.pez.pezaudioplayer.music.StateMediaPlayer;

/**
 * Created by nicolas on 07/11/2015.
 */
public class SeekbarUpdate extends AsyncTask<StateMediaPlayer, Integer, Integer> {

    private SeekBar seekBar;
    private boolean isOver = false;

    public SeekbarUpdate(SeekBar seekBar) {
        this.seekBar = seekBar;
    }

    @Override
    protected Integer doInBackground(StateMediaPlayer... params) {
        if (isOver || seekBar == null) {
            return null;
        }

        StateMediaPlayer mp = params[0];

        while(!isOver && mp.isState_Or(MusicPlayerState.IDLE, MusicPlayerState.PREPARING, MusicPlayerState.INITIALIZED)) {
            //Wait for mediaplayer to prepare the song
        }
        while (!isOver && !mp.isState_Or(MusicPlayerState.PLAYBACK_COMPLETED, MusicPlayerState.IDLE, MusicPlayerState.INITIALIZED, MusicPlayerState.ERROR, MusicPlayerState.STOPPED, MusicPlayerState.PREPARING) ) {
            publishProgress(mp.getCurrentPosition(), mp.getDuration());
        }
//        while (params[0].getCurrentPosition() != params[0].getDuration()){
//            publishProgress(params[0].getCurrentPosition());
//        }
        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        seekBar.setProgress(values[0]);
        seekBar.setMax(values[1]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
    }

    public void stop() {
        isOver = true;
    }
}

package com.isen.pez.pezaudioplayer.fragments.playlists_fragment;

import android.app.Fragment;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MusicActivity;
import com.isen.pez.pezaudioplayer.music.MusicLibrary;

/**
 * Created by ronan on 11/11/15.
 */
public class PlaylistsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private RecyclerView mRecyclerView;
    private MusicActivity mActivity;
    private MusicLibrary mMusicLibrary;
    private PlaylistsCursorAdapter mAdapter;
    private Long mPlaylistId = null;

    public PlaylistsFragment() {
        // Required empty public constructor
    }

    public static PlaylistsFragment newInstance(MusicActivity activity) {
        Log.i("newPlaylistsFrag", "");
        PlaylistsFragment fragment = new PlaylistsFragment();
        fragment.mActivity = activity;
        fragment.mMusicLibrary = activity.getMusicLibrary();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) inflater.inflate(R.layout.songs_fragment, container, false);//tmp
        mRecyclerView.setLayoutManager(new GridLayoutManager(PezApplication.getContext(), 2));
        setupRecyclerView(mRecyclerView);
        getLoaderManager().restartLoader(0, null, this);
        return mRecyclerView;
    }

    public void setupRecyclerView(RecyclerView mRecyclerView) {
        this.mRecyclerView = mRecyclerView;
        mAdapter = null;
        setAdapter(null);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;
        cursorLoader = mMusicLibrary.initPlaylistCursor();

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.i("onLoadFinished",""+data);
        setAdapter(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    private void setAdapter(Cursor data) {
        if (mAdapter != null) {
            mAdapter.changeCursor(data);
        } else {
            mAdapter = new PlaylistsCursorAdapter(mActivity.getApplicationContext(), data, mActivity);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}

package com.isen.pez.pezaudioplayer.music;

/**
 * Created by franc on 06/11/2015.
 */
import android.content.ContentResolver;
import android.content.CursorLoader;
import android.database.Cursor;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.AudioColumns;
import android.provider.MediaStore.Audio.ArtistColumns;
import android.provider.MediaStore.Audio.AlbumColumns;
import android.provider.MediaStore.Audio.PlaylistsColumns;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.model.*;

import java.util.ArrayList;

public class MusicLibrary {

    private CursorLoader data;

    public MusicLibrary() {
    }

    // SONGS

    public final CursorLoader initSongsCursor() {
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[]{
                        BaseColumns._ID,
                        AudioColumns.TITLE,
                        AudioColumns.ARTIST,
                        AudioColumns.ALBUM,
                        AudioColumns.DATA,
                        AudioColumns.DURATION,
                        AudioColumns.ALBUM_ID
                },null,null,MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }

    // ARTISTS

    public final CursorLoader initArtistCursor() {
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                new String[] {
                        BaseColumns._ID,
                        ArtistColumns.ARTIST,
                        ArtistColumns.NUMBER_OF_ALBUMS,
                        ArtistColumns.NUMBER_OF_TRACKS
                }, null, null, MediaStore.Audio.Artists.DEFAULT_SORT_ORDER);
    }

    //ALBUMS

    public final CursorLoader initAlbumCursor() {
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[] {
                        BaseColumns._ID,
                        AlbumColumns.ALBUM,
                        AlbumColumns.ARTIST,
                        AlbumColumns.NUMBER_OF_SONGS,
                        AlbumColumns.FIRST_YEAR,
                        AlbumColumns.ALBUM_ART
                }, null, null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
    }

    //ALBUMS BY ARTIST

    public final CursorLoader initArtistAlbumCursor(final Long artistId) {
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Artists.Albums.getContentUri("external", artistId),
                new String[] {
                        BaseColumns._ID,
                        AlbumColumns.ALBUM,
                        AlbumColumns.ARTIST,
                        AlbumColumns.NUMBER_OF_SONGS,
                        AlbumColumns.FIRST_YEAR,
                        AlbumColumns.ALBUM_ART
                }, null, null, MediaStore.Audio.Albums.DEFAULT_SORT_ORDER);
    }

    //SONGS BY ARTIST

    public final CursorLoader initArtistSongCursor(final Long artistId) {
        final StringBuilder selection = new StringBuilder();
        selection.append(AudioColumns.IS_MUSIC + "=1");
        selection.append(" AND " + AudioColumns.TITLE + " != ''");
        selection.append(" AND " + AudioColumns.ARTIST_ID + "=" + artistId);
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] {
                        BaseColumns._ID,
                        AudioColumns.TITLE,
                        AudioColumns.ARTIST,
                        AudioColumns.ALBUM,
                        AudioColumns.DATA,
                        AudioColumns.DURATION
                }, selection.toString(), null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }

    //SONGS BY ALBUM

    public final CursorLoader initAlbumSongCursor(final Long albumId) {
        final StringBuilder selection = new StringBuilder();
        selection.append(AudioColumns.IS_MUSIC + "=1");
        selection.append(" AND " + AudioColumns.TITLE + " != ''");
        selection.append(" AND " + AudioColumns.ALBUM_ID + "=" + albumId);
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] {
                        BaseColumns._ID,
                        AudioColumns.TITLE,
                        AudioColumns.ARTIST,
                        AudioColumns.ALBUM,
                        AudioColumns.DATA,
                        AudioColumns.DURATION,
                }, selection.toString(), null, MediaStore.Audio.Media.TRACK + ", " + MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }

    //PLAYLISTS

    public final CursorLoader initPlaylistCursor() {
        return new CursorLoader(PezApplication.getContext(), MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{
                        BaseColumns._ID,
                        PlaylistsColumns.NAME,
                        PlaylistsColumns.DATE_MODIFIED
                },null,null,MediaStore.Audio.Playlists.DEFAULT_SORT_ORDER);
    }

    //SONGS BY PLAYLISTS

    public final CursorLoader initPlaylistSongCursor(final Long playlistId) {
        final StringBuilder selection = new StringBuilder();
        selection.append(AudioColumns.IS_MUSIC + "=1");
        selection.append(" AND " + AudioColumns.TITLE + " != ''");
        //selection.append(" AND " + AudioColumns. + "=" + playlistId);
        return new CursorLoader(PezApplication.getContext(),MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] {
                        BaseColumns._ID,
                        AudioColumns.TITLE,
                        AudioColumns.ARTIST,
                        AudioColumns.ALBUM,
                        AudioColumns.DATA,
                        AudioColumns.DURATION,
                }, selection.toString(), null, MediaStore.Audio.Media.TRACK + ", " + MediaStore.Audio.Media.DEFAULT_SORT_ORDER);
    }
}
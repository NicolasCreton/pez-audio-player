package com.isen.pez.pezaudioplayer.utils;

/**
 * Created by cdupl on 11/7/2015.
 */
public class Constants {
    public class MusicPlayer {
        public static final String PLAY_LIST_SONGS_ACTION = "play_list_songs_action";
        public static final String PLAY_ACTION = "play_action";
        public static final String PLAY_FILE_PATH_ACTION = "play_file_path_action";
        public static final String PLAY_ALBUM_ACTION = "play_album_action";
        public static final String PLAY_ARTIST_ACTION = "play_artist_action";
        public static final String PAUSE_ACTION = "pause_action";
        public static final String TOGGLE_PLAY_PAUSE_ACTION = "toggle_play_pause_action";

        public static final String PLAY_SONG_IN_QUEUE = "play_song_in_queue";
    }

    public class BroadcastConstants{
        public static final String PLAY = "play_broad";
        public static final String PAUSE = "pause_broad";
    }
}

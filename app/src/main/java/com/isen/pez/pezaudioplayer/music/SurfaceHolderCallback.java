package com.isen.pez.pezaudioplayer.music;

import android.view.SurfaceHolder;

/**
 * Created by cdupl on 11/7/2015.
 */
public class SurfaceHolderCallback implements SurfaceHolder.Callback {

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        MusicPlayer.getMediaPlayer().setDisplay(MusicPlayer.getSurfaceView().getHolder());
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}

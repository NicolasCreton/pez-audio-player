package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistSearch;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by cdupl on 11/7/2015.
 */
public class WebArtistResponse {
    @SerializedName("response")
    public WebResponse response;

    @SerializedName("start")
    public String start;

    @SerializedName("total")
    public String total;

    @SerializedName("images")
    public ArrayList<EchoNestImages> images = new ArrayList<EchoNestImages>();
}

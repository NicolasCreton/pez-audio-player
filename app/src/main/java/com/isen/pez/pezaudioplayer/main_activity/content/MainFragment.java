package com.isen.pez.pezaudioplayer.main_activity.content;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.main_activity.MainActivity;

import java.io.File;

/**
 * Created by cdupl on 11/6/2015.
 */
public class MainFragment extends Fragment {
    private MainActivity mMainActivity;

    private View rootView;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private MyPagerAdapter pagerAdapter;

    public static MainFragment newInstance(MainActivity mainActivity) {
        MainFragment fragment = new MainFragment();
        fragment.setActivity(mainActivity);
        return fragment;
    }

    public void setActivity(MainActivity mMainActivity) {
        this.mMainActivity = mMainActivity;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tabs_fragment, container, false);
        Toolbar tbar = (Toolbar)rootView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(tbar);
        //tbar.setTitle("Pez Audio Player");
        tabLayout = (TabLayout)rootView.findViewById(R.id.tabLayout);
        viewPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        initViewPagerAndTabs();
        return rootView;
    }
    private void initViewPagerAndTabs() {

        tabLayout.addTab(tabLayout.newTab().setText("File Explorer"));
        tabLayout.addTab(tabLayout.newTab().setText("Artists"));
        tabLayout.addTab(tabLayout.newTab().setText("Albums"));
        tabLayout.addTab(tabLayout.newTab().setText("Songs"));
        //tabLayout.addTab(tabLayout.newTab().setText("Playlists"));

        //tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);


        pagerAdapter = new MyPagerAdapter(getChildFragmentManager(), mMainActivity);

        viewPager.setAdapter(pagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public MyPagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }

    public TabLayout getTabLayout() {
        return tabLayout;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }

    public void setNewDirectory(File newDirectory) {
        pagerAdapter.setNewDirectory(newDirectory);
    }
}

package com.isen.pez.pezaudioplayer.interfaces;

import java.io.File;

/**
 * Created by cdupl on 11/6/2015.
 */
public interface DirectoryClickListener {
    void onDirectoryClick(String dirPath);
    void onDirectoryClick(File folder);
    void playSong(File file);
}

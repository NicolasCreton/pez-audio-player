package com.isen.pez.pezaudioplayer.web_retriever.EchoNestWebRetriever.ArtistBiography;

import com.google.gson.annotations.SerializedName;

/**
 * Created by cdupl on 11/7/2015.
 */
public class Biography {
    @SerializedName("text")
    public String text;

    @SerializedName("site")
    public String site;

    @SerializedName("url")
    public String url;
}

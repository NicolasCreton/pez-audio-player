package com.isen.pez.pezaudioplayer.asyncTask;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.ImageView;

import com.isen.pez.pezaudioplayer.PezApplication;
import com.isen.pez.pezaudioplayer.PicassoHelper;
import com.isen.pez.pezaudioplayer.R;
import com.isen.pez.pezaudioplayer.model.Artist;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by franc on 11/11/2015.
 */
public class ArtistArt  extends AsyncTask<Void, Void, String> {
    private Artist mArtist;

    public ArtistArt(Artist artist) {
        mArtist = artist;
    }

    @Override
    protected String doInBackground(Void... params) {

        String imageUrl = null;
        try {
            URL url = new URL("https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=" + mArtist.mArtistName.replace(" ", "+") + "+musician+photo&rsz=1&tbs=isz:m&tbm=isch");
            URLConnection connection = url.openConnection();
            String line;
            StringBuilder builder = new StringBuilder();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            JSONObject json = new JSONObject(builder.toString());
            JSONObject json2 = json.getJSONObject("responseData");
            JSONArray json3 = json2.getJSONArray("results");
            JSONObject json4 = json3.getJSONObject(0);
            imageUrl = json4.getString("unescapedUrl");
        } catch (MalformedURLException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        } catch (JSONException e){
            e.printStackTrace();
        }
        return imageUrl;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s!=null) {
            mArtist.setImagePath(s);
        }
    }

}
